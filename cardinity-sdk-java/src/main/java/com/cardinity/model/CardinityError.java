package com.cardinity.model;

import java.util.Set;

/**
 * <p>CardinityError class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class CardinityError {

    private String type;
    private String title;
    private Integer status;
    private String detail;
    private Set<ErrorItem> errors;

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getType() {
        return type;
    }

    /**
     * <p>Getter for the field <code>title</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getTitle() {
        return title;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getStatus() {
        return status;
    }

    /**
     * <p>Getter for the field <code>detail</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDetail() {
        return detail;
    }

    /**
     * <p>getErrorItems.</p>
     *
     * @return a {@link java.util.Set} object.
     */
    public Set<ErrorItem> getErrorItems() {
        return errors;
    }

}
