package com.cardinity.rest;

import com.cardinity.model.Result;
import com.google.gson.reflect.TypeToken;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * <p>RestClient interface.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public interface RestClient {

    /**
     * <p>sendRequest.</p>
     *
     * @param method a {@link com.cardinity.rest.RestResource.RequestMethod} object.
     * @param url a {@link java.lang.String} object.
     * @param clazz a {@link com.google.gson.reflect.TypeToken} object.
     * @param requestObject a T object.
     * @param <T> a T object.
     * @return a {@link com.cardinity.model.Result} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    <T> Result<T> sendRequest(RestResource.RequestMethod method, String url, TypeToken<T> clazz, T requestObject) throws GeneralSecurityException, UnsupportedEncodingException;

    /**
     * <p>sendRequest.</p>
     *
     * @param method a {@link com.cardinity.rest.RestResource.RequestMethod} object.
     * @param url a {@link java.lang.String} object.
     * @param clazz a {@link com.google.gson.reflect.TypeToken} object.
     * @param <T> a T object.
     * @return a {@link com.cardinity.model.Result} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    <T> Result<T> sendRequest(RestResource.RequestMethod method, String url, TypeToken<T> clazz) throws GeneralSecurityException, UnsupportedEncodingException;

    /**
     * <p>sendRequest.</p>
     *
     * @param method a {@link com.cardinity.rest.RestResource.RequestMethod} object.
     * @param url a {@link java.lang.String} object.
     * @param clazz a {@link com.google.gson.reflect.TypeToken} object.
     * @param params a {@link java.util.Map} object.
     * @param <T> a T object.
     * @return a {@link com.cardinity.model.Result} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    <T> Result<T> sendRequest(RestResource.RequestMethod method, String url, TypeToken<T> clazz, Map<String, String>
            params) throws GeneralSecurityException, UnsupportedEncodingException;

}
