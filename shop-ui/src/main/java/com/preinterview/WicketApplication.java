package com.preinterview;

import com.cardinity.model.Payment;
import com.preinterview.ui.payment.PaymentHandler;
import com.preinterview.ui.shopui.AppContextToCssUrlAppender;
import com.preinterview.ui.shopui.JavaScriptToBodyResponseDecorator;
import com.preinterview.ui.shopui.JsAndCssResources;
import com.preinterview.ui.shopui.TripleViewPage;
import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.protocol.http.WebApplication;
import org.apache.wicket.request.resource.IResource;
import org.apache.wicket.request.resource.JavaScriptResourceReference;
import org.apache.wicket.request.resource.ResourceReference;
import org.apache.wicket.resource.CssUrlReplacer;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class WicketApplication extends WebApplication {
    /**
     * @see org.apache.wicket.Application#getHomePage()
     */

    /**
     * naiveCustomerDB is a database like list to save customers, where
     * customer = customerId = current session id
     * */
    public static List<String> naiveCustomerDB;

    /**
     * naiveCustomerToTransactionDB is a database like list to save customer
     * and his relation to his performed transactions.
     *
     * (customer, transactionId), i.e
     * ("607C13BC3330178885587516811E2701",
     * "f0000213-6bec-4648-8b5c-f5982572e18a")
     *
     * */
    public static Map<String, List<String>> naiveCustomerToTransactionDB;

    /**
     * naiveTransactionStatusDB is a database like structure to save
     * transaction and transactions current status
     *
     * (transactionId, Payment.Status), i.e
     * ("f0000213-6bec-4648-8b5c-f5982572e18a", Payment.Status.APPROVED)
     * */
    public static Map<String, Payment.Status> naiveTransactionStatusDB;

    /**
     * naiveTransactionAuthorizationDB is a database like structure to save
     * transaction and transaction's authorization data
     *
     * (transactionId, authorization), i.e
     * ("f0000213-6bec-4648-8b5c-f5982572e18a", "3d-pass"), or
     * ("f0000213-6bec-4648-8b5c-f5982572e18a", "3d-fail")
     * */
    public static Map<String, String> naiveTransactionAuthorizationDB;

    /**
     * Our callback url to which cardinity api will return authorization
     * shiz in non JSON compliant form...LOL.
     * */
    public static String callBackUrl =
            "http://localhost:8080/api/processor/process";

    /**
     * consumer key supplied by cardinity people
     * */
    public static String consumerKey =
            "test_3a4393c3da1a4e316ee66c0cc61c71";

    /**
     * consumer secret supplied by cardinity people
     * */
    public static String consumerSecret =
            "ffe1372c074185b19c309964812bb8f3f2256ba514aea8a318";

    @Override
    public Class<? extends WebPage> getHomePage() {
        return TripleViewPage.class;
    }

    /**
     * @see org.apache.wicket.Application#init()
     */
    @Override
    public void init() {
        super.init();
        naiveCustomerDB = new ArrayList<>();
        naiveCustomerToTransactionDB = new HashMap<>();
        naiveTransactionStatusDB = new HashMap<>();
        naiveTransactionAuthorizationDB = new HashMap<>();
        getHeaderResponseDecorators()
                .add(new JavaScriptToBodyResponseDecorator(
                        "footer-container"));

        JavaScriptResourceReference wicketJQuery =
                (JavaScriptResourceReference) getJavaScriptLibrarySettings()
                        .getJQueryReference();

        JavaScriptResourceReference wicketAjax =
                (JavaScriptResourceReference) getJavaScriptLibrarySettings()
                        .getWicketAjaxReference();

        getResourceBundles().addJavaScriptBundle(WicketApplication.class,
                "bundle.js",
                wicketJQuery,
                wicketAjax,
                JsAndCssResources.jqueryUiUiMinJsReference,
                JsAndCssResources.jqueryUiMinJsReference,
                JsAndCssResources.dataTablesMinJsReference,
                JsAndCssResources.dataTablesAdoptionJsReference,
                JsAndCssResources.jqueryUiLayoutAdoption
        );

        getResourceBundles().addCssBundle(WicketApplication.class,
                "bundle.css",
                JsAndCssResources.dataTablesCssMinReference,
                JsAndCssResources.jqueryUiLayoutDefaultCss,
                JsAndCssResources.formDefaultCss);

        /**to solve image css url path resolution problem
         *
         * home made nifty grifty solution
         * need to document on stackoverflow!!!!
         */
        getResourceSettings()
                .setCssCompressor(new AppContextToCssUrlAppender());

        /**a very very very very very very very very very ver very very very
        very very very very very very very very very ver very very very very
         very very very very very very very very very ver very very very very
         very very very very very very very very very ver very very very very
         very very very very very very very very very ver very very very very
         very very very very very very very very very ver very very very very
         very very very very very very very very very ver very very very very
         BAD PRACTICE. We are disabling csp because we are lazy to

         1. write style sheet for a ModalDialog, and so we are using legacy
            class modal dialog which gives errors such as

         error:

         Refused to apply inline style because it violates the following
         Content Security Policy directive: "style-src
         'nonce-yXAzcxrSLkqBVi6fADB0EFH3'". Either the 'unsafe-inline'
         keyword, a hash ('sha256-aqNNdDLnnrDOnTNdkJpYlAxKVJtLt9CtFLklmInu
         UAE='), or a nonce ('nonce-...') is required to enable
         inline execution.

         solution use ModalDialog instead of ModalWindow an add few more extra
         css classes (not css style elements!!!!!)

         2. we are lazy to exclude cardinity cas server from frame-src policy
            and because of it we disable all CSP ='[.

         error:

         Refused to frame 'https://acs.cardinity.com/' because it
         violates the following Content Security Policy directive:
         "frame-src 'self'".

         solution:

         getCspSettings().blocking().add(CSPDirective.FRAME_SRC,
         "https://*.cardinity.com/");

         BUT F IT.
         */
        getCspSettings().blocking().disabled();


        mountResource("/api", new ResourceReference("restReference") {
            PaymentHandler resource = new PaymentHandler();

            @Override
            public IResource getResource() {
                return resource;
            }
        });

    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return RuntimeConfigurationType.DEPLOYMENT;
    }
}
