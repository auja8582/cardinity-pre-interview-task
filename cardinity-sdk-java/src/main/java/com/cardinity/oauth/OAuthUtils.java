package com.cardinity.oauth;

import com.cardinity.Cardinity;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

/**
 * <p>OAuthUtils class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public final class OAuthUtils {

    private OAuthUtils() {
    }

    /**
     * <p>percentEncode.</p>
     *
     * @param s a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String percentEncode(String s) {
        if (s == null) {
            return "";
        }
        try {
            return URLEncoder.encode(s, Cardinity.ENCODING_CHARSET).replace("+", "%20").replace("*", "%2A")
                    .replace("%7E", "~");
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getMessage(), ex);
        }
    }
}
