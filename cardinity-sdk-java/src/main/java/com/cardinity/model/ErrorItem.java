package com.cardinity.model;

/**
 * <p>ErrorItem class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class ErrorItem {

    private String field;
    private String rejected;
    private String message;

    /**
     * <p>Getter for the field <code>field</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getField() {
        return field;
    }

    /**
     * <p>Getter for the field <code>rejected</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getRejected() {
        return rejected;
    }

    /**
     * <p>Getter for the field <code>message</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getMessage() {
        return message;
    }

}
