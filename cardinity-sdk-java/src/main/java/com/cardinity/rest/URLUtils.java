package com.cardinity.rest;

import com.cardinity.Cardinity;
import com.cardinity.rest.RestResource.Resource;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.UUID;

/**
 * <p>URLUtils class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public final class URLUtils {

    private final static String URL_SEPARATOR = "/";

    private URLUtils() {

    }

    /**
     * <p>buildUrl.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public static String buildUrl() {
        return buildCardinityUrl(null, null, null);
    }

    /**
     * <p>buildUrl.</p>
     *
     * @param paymentId a {@link java.util.UUID} object.
     * @return a {@link java.lang.String} object.
     */
    public static String buildUrl(UUID paymentId) {
        return buildCardinityUrl(paymentId, null, null);
    }

    /**
     * <p>buildUrl.</p>
     *
     * @param paymentId a {@link java.util.UUID} object.
     * @param resource a {@link com.cardinity.rest.RestResource.Resource} object.
     * @return a {@link java.lang.String} object.
     */
    public static String buildUrl(UUID paymentId, Resource resource) {
        return buildCardinityUrl(paymentId, resource, null);
    }

    /**
     * <p>buildUrl.</p>
     *
     * @param paymentId a {@link java.util.UUID} object.
     * @param resource a {@link com.cardinity.rest.RestResource.Resource} object.
     * @param actionID a {@link java.util.UUID} object.
     * @return a {@link java.lang.String} object.
     */
    public static String buildUrl(UUID paymentId, Resource resource, UUID actionID) {
        return buildCardinityUrl(paymentId, resource, actionID);
    }

    private static String buildCardinityUrl(UUID paymentId, Resource action, UUID actionId) {

        StringBuilder url = new StringBuilder(Cardinity.API_BASE).append(URL_SEPARATOR).append(Cardinity.API_VERSION)
                .append(URL_SEPARATOR).append(Resource.PAYMENTS);

        if (paymentId != null) {
            url.append(URL_SEPARATOR);
            url.append(paymentId);
            if (action != null) {
                url.append(URL_SEPARATOR);
                url.append(action);
                if (actionId != null) {
                    url.append(URL_SEPARATOR);
                    url.append(actionId);
                }
            }
        }
        return url.toString().toLowerCase();
    }

    /**
     * <p>formatURL.</p>
     *
     * @param url a {@link java.lang.String} object.
     * @param query a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     */
    public static String formatURL(String url, String query) {
        if (query == null || query.isEmpty()) {
            return url;
        } else {
            String separator = url.contains("?") ? "&" : "?";
            return String.format("%s%s%s", url, separator, query);
        }
    }

    /**
     * <p>buildQueryParam.</p>
     *
     * @param key a {@link java.lang.String} object.
     * @param value a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    public static String buildQueryParam(String key, String value) throws UnsupportedEncodingException {
        return String.format("%s=%s", URLEncoder.encode(key, Cardinity.ENCODING_CHARSET), URLEncoder.encode(value,
                Cardinity.ENCODING_CHARSET));
    }
}
