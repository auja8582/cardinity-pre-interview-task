package com.cardinity.model;

/**
 * <p>Card class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Card implements PaymentInstrument {

    private String cardBrand;
    private String pan;
    private Integer expYear;
    private Integer expMonth;
    private Integer cvc;
    private String holder;

    /**
     * <p>Getter for the field <code>cardBrand</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCardBrand() {
        return cardBrand;
    }

    /**
     * <p>Getter for the field <code>pan</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPan() {
        return pan;
    }

    /**
     * <p>Setter for the field <code>pan</code>.</p>
     *
     * @param pan a {@link java.lang.String} object.
     */
    public void setPan(String pan) {
        this.pan = pan;
    }

    /**
     * <p>Getter for the field <code>expYear</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getExpYear() {
        return expYear;
    }

    /**
     * <p>Setter for the field <code>expYear</code>.</p>
     *
     * @param expYear a {@link java.lang.Integer} object.
     */
    public void setExpYear(Integer expYear) {
        this.expYear = expYear;
    }

    /**
     * <p>Getter for the field <code>expMonth</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getExpMonth() {
        return expMonth;
    }

    /**
     * <p>Setter for the field <code>expMonth</code>.</p>
     *
     * @param expMonth a {@link java.lang.Integer} object.
     */
    public void setExpMonth(Integer expMonth) {
        this.expMonth = expMonth;
    }

    /**
     * <p>Setter for the field <code>cvc</code>.</p>
     *
     * @param cvc a {@link java.lang.Integer} object.
     */
    public void setCvc(Integer cvc) {
        this.cvc = cvc;
    }

    /**
     * <p>Getter for the field <code>holder</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getHolder() {
        return holder;
    }

    /**
     * <p>Setter for the field <code>holder</code>.</p>
     *
     * @param holder a {@link java.lang.String} object.
     */
    public void setHolder(String holder) {
        this.holder = holder;
    }

    /**
     * <p>Getter for the field <code>cvc</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getCvc() {
        return cvc;
    }

    /** {@inheritDoc} */
    @Override
    public Payment.Method getType() {
        return Payment.Method.CARD;
    }
}
