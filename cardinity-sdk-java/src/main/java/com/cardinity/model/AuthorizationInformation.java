package com.cardinity.model;

/**
 * <p>AuthorizationInformation class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class AuthorizationInformation {

    private String url;
    private String data;

    /**
     * <p>Getter for the field <code>url</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getUrl() {
        return url;
    }

    /**
     * <p>Getter for the field <code>data</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getData() {
        return data;
    }

}
