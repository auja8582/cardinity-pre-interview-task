module cardinity.sdk.java {
    exports com.cardinity.model;
    exports com.cardinity.client;
    requires com.google.gson;
    requires org.apache.commons.codec;
    requires org.apache.httpcomponents.httpclient;
    requires org.apache.httpcomponents.httpcore;
    opens com.cardinity.model to com.google.gson;
    exports com.cardinity.exceptions;
}