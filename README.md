# README #

This README would normally document whatever steps are necessary to get your application up and running.
Also, it shows the basics on how to do 3D Secure Payment via cardinity api using Wicket 9.x API.

The aftermath

applied for https://bitbucket.org/auja8582/cardinity-pre-interview-task/downloads/Job_ad.txt  
position. Did the task. Heard from them back. Had issues with me hardcoding keys into source code=].
Assured me that they will review the code and will call me back, but never did.

### What is this repository for? ###

* Solution to the given pre interview task (https://bitbucket.org/auja8582/cardinity-pre-interview-task/downloads/atranka_-_JAVA_programuotojas.pdf)
* 1.0
* cardinity payment api url: https://developers.cardinity.com/api/v1/#testing

### How do I get set up? ###

* Check out this repository using git clone https://auja8582@bitbucket.org/auja8582/cardinity-pre-interview-task.git
* From parent directory run mvn clean install tomcat7:run and go to http://localhost:8080
* No other dependencies are needed
* No databases in this project
* No Docker

### P.S ###

App was tested only on tomcat 7 plugin.
You might encounter errors such as 

	SEVERE: Unable to process Jar entry [module-info.class] from Jar [/target/shop-ui-1.0-SNAPSHOT/WEB-INF/lib/gson-2.8.6.jar!/] for annotations
	org.apache.tomcat.util.bcel.classfile.ClassFormatException: Invalid byte tag in constant pool: 19
		at org.apache.tomcat.util.bcel.classfile.Constant.readConstant(Constant.java:133)
		at org.apache.tomcat.util.bcel.classfile.ConstantPool.<init>(ConstantPool.java:60)
		at org.apache.tomcat.util.bcel.classfile.ClassParser.readConstantPool(ClassParser.java:209)
		at org.apache.tomcat.util.bcel.classfile.ClassParser.parse(ClassParser.java:119)

These errors are considered "normal" as we use java 11 with tomcat7 maven plugin which does not fully support
java modularity. To not see these errors you can generate *.war file and run it on tomcat 8 or 9 version (you will 
have to change dependency scope log4j-slf4j-impl from test to compile in shop-ui pom.xml). ~~Be aware that if you decide
to install it as a war to a standalone server it has to be installed as a ROOT.war, otherwise sorting icons disappear.
You can avoid this inconvenience by mounting datatables/image folder to server as a workaround or rename war to ROOT.war.
~~ (fixed in 2020-05-07 10:29 PM (UTC))


OR

    WARNING: datatables.js:11913: WARNING - Parse error. illegal use of unknown JSDoc tag "dtopt"; ignoring it
		 *  @dtopt Options
		    ^
These warnings are from minifier maven plugin, happens when minifying files. And are considered normal.

There are also three values that you can adjust (adjust these values in WicketApplication.java file)

1. ***callBackUrl***, the default value is "http://localhost:8080/api/processor/process", adjust this value to
   match your server, i.e [server_url]:[port_number]/[artifact-id]/api/processor/process. This callBackUrl receives 
   payment authorization response from cardinity cas server. I.e http://my.server/app-name/api/processor/process or
   http://localhost:8089/shop-ui-1.0-SNAPSHOT/api/processor/process if deployed on the seperate server.

2. ***consumerKey***, the default value was provided by cardinity people and is set to 
   test environment. Change it as you see fit.

3. ***consumerSecret***, the default value was provided by cardinity people and is set to 
   test environment. Change it as you see fit.


App works only with test environment consumer key and test environment consumer secret (are hardcoded in WicketApplication.java file)
for convenience sake. But should be externalized i.e Spring Vault + Spring Config.

### UNRELATED NOTES ###

It feels that processing cvc value has a small issue. Cardinity api accepts cvc values in all lengths
if padding zeroes from the left side, .i.e accepted values

    1
	11
	111
	00000000000000000000000011
    00000000000000000000000111
	
All these values are legal while using cardinity API. Because they parse cvc value to ant Integer and 
not a String.

if we went to https://github.com/cardinity/cardinity-sdk-java page, we would see that they claim that their
cardinity-sdk-java (version: 1.1.0) is working with jdk 1.6 or later... Well, that is only  partially true... 
If you tried to run on java 9 or later with modular environment you would definitely get stuck. Maybe you 
could update your API? For a very naive update solution, please, refer to cardinity-sdk-java subproject, 
which is a part of this project.