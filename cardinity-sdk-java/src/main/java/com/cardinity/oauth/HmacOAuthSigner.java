package com.cardinity.oauth;

import com.cardinity.Cardinity;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;


/**
 * <p>HmacOAuthSigner class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class HmacOAuthSigner implements OAuthSigner {

    /** {@inheritDoc} */
    @Override
    public String getSignatureMethod() {
        return "HMAC-SHA1";
    }

    /** {@inheritDoc} */
    @Override
    public String computeSignature(String signatureBaseString, String consumerSecret) throws
            GeneralSecurityException, UnsupportedEncodingException {

        String key = OAuthUtils.percentEncode(consumerSecret) + "&";
        SecretKey secretKey = new SecretKeySpec(key.getBytes(Cardinity.ENCODING_CHARSET), "HmacSHA1");
        Mac mac = Mac.getInstance("HmacSHA1");
        mac.init(secretKey);
        return new Base64().encodeAsString(mac.doFinal(signatureBaseString.getBytes(Cardinity
                .ENCODING_CHARSET)));
    }
}
