package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Parcel;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.IValidator;
import org.apache.wicket.validation.ValidationError;

public class OutOfStockValidator implements IValidator {

    @Override
    public void validate(IValidatable validatable) {

        if (validatable.getValue() == null) {
            error(validatable , "test");
        } else if(((Boolean) validatable.getValue()) == false){
            error(validatable, "out-of-stock");
        }

    }

    private void error(IValidatable<Parcel> validatable, String errorKey) {
        ValidationError error = new ValidationError();
        error.addKey(getClass().getSimpleName() + "." + errorKey);
        validatable.error(error);
    }

}
