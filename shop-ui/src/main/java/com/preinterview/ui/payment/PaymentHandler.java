package com.preinterview.ui.payment;

import com.preinterview.WicketApplication;
import org.wicketstuff.rest.annotations.MethodMapping;
import org.wicketstuff.rest.annotations.parameters.RequestBody;
import org.wicketstuff.rest.contenthandling.mimetypes.RestMimeTypes;
import org.wicketstuff.rest.contenthandling.webserialdeserial.TextualWebSerialDeserial;
import org.wicketstuff.rest.resource.AbstractRestResource;
import org.wicketstuff.restutils.http.HttpMethod;

import java.nio.charset.StandardCharsets;


public class PaymentHandler extends
        AbstractRestResource<TextualWebSerialDeserial> {

    public PaymentHandler() {
        super(new TextualWebSerialDeserial(StandardCharsets.UTF_8.name(),
                RestMimeTypes.APPLICATION_JSON,
                new FormDataDeserializer()));
    }

    @MethodMapping(value = "/processor/process", httpMethod = HttpMethod.POST)
    public String process(@RequestBody String md) {
        String authorizationStatus = md.split("&")[0].split("=")[1];
        String transactionId = md.split("&")[1].split("=")[1];
        WicketApplication.naiveTransactionAuthorizationDB.put(transactionId,
                authorizationStatus);
        return "You can close this window now.";
    }
}
