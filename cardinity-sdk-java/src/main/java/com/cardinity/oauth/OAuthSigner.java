package com.cardinity.oauth;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;

/**
 * <p>OAuthSigner interface.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public interface OAuthSigner {

    /**
     * <p>getSignatureMethod.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    String getSignatureMethod();

    /**
     * <p>computeSignature.</p>
     *
     * @param signatureBaseString a {@link java.lang.String} object.
     * @param consumerSecret a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    String computeSignature(String signatureBaseString, String consumerSecret) throws GeneralSecurityException,
            UnsupportedEncodingException;
}
