package com.cardinity.model;

/**
 * <p>Result class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Result<T> {

    private T item;
    private CardinityError cardinityError;

    /**
     * <p>Constructor for Result.</p>
     *
     * @param item a T object.
     */
    public Result(T item) {
        this.item = item;
    }

    /**
     * <p>Constructor for Result.</p>
     *
     * @param cardinityError a {@link com.cardinity.model.CardinityError} object.
     */
    public Result(CardinityError cardinityError) {
        this.cardinityError = cardinityError;
    }

    /**
     * <p>Getter for the field <code>item</code>.</p>
     *
     * @return a T object.
     */
    public T getItem() {
        return item;
    }

    /**
     * <p>Getter for the field <code>cardinityError</code>.</p>
     *
     * @return a {@link com.cardinity.model.CardinityError} object.
     */
    public CardinityError getCardinityError() {
        return cardinityError;
    }

    /**
     * <p>isValid.</p>
     *
     * @return a boolean.
     */
    public boolean isValid() {
        return item != null;
    }
}
