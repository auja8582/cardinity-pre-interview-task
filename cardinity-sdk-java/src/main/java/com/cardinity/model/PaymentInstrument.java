package com.cardinity.model;

/**
 * <p>PaymentInstrument interface.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public interface PaymentInstrument {

    /**
     * <p>getType.</p>
     *
     * @return a {@link com.cardinity.model.Payment.Method} object.
     */
    Payment.Method getType();

}
