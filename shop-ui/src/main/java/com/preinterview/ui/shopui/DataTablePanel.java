package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Parcel;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.PropertyColumn;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;

import java.util.ArrayList;
import java.util.List;

public class DataTablePanel extends Panel {

    private final List<IColumn<Parcel, String>> columns = new ArrayList<>();

    private final IColumn<Parcel, String> ido =
            new PropertyColumn<>(Model.of("id "), "id");
    private final IColumn<Parcel, String> name =
            new PropertyColumn<>(Model.of("name "), "name");
    private final IColumn<Parcel, String> description =
            new PropertyColumn<>(Model.of("description "), "description");
    private final IColumn<Parcel, String> price =
            new PropertyColumn<>(Model.of("price "), "price");
    private final IColumn<Parcel, String> vat =
            new PropertyColumn<>(Model.of("vat "), "vat");
    private final IColumn<Parcel, String> availableInWareHouse =
            new PropertyColumn<>(Model.of("available "), "availableInWareHouse");


    private SortableParcelDataProvider dataProvider =
	new SortableParcelDataProvider();

    private DataTable<Parcel, String> dataTable =
            new DefaultDataTableExtended("table", columns, dataProvider, 50);

    public DataTablePanel(String id) {
        super(id);
        initializeTable();
        dataTable.setOutputMarkupId(true);
        dataTable.setMarkupId("table");
        add(dataTable);
    }

    private void initializeTable(){
        columns.add(ido);
        columns.add(name);
        columns.add(price);
        columns.add(vat);
        columns.add(availableInWareHouse);
        columns.add(description);
    }
}
