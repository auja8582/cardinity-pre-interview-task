package com.cardinity.validators;

/**
 * <p>Validator interface.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public interface Validator<T> {

    /**
     * <p>validate.</p>
     *
     * @param object a T object.
     */
    void validate(T object);
}
