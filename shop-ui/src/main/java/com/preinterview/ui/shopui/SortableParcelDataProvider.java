package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Parcel;
import org.apache.wicket.extensions.markup.html.repeater.data.sort.SortOrder;
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.LambdaModel;

import java.util.Iterator;
import java.util.List;

public class SortableParcelDataProvider extends
        SortableDataProvider<Parcel, String>{

    public SortableParcelDataProvider() {
        setSort("name", SortOrder.ASCENDING);
    }

    //a piece of data for fast testing
    private List<Parcel> getData() {
        List<Parcel> parcels = List.of(
                new Parcel(1L, "Lamp", "Made in China. date 2019-02-01", "€20.15", "€2", true),
                new Parcel(2L, "Green Lamp", "Made in China. date 2007-05-01", "€20.15", "€2", true),
                new Parcel(3L, "Yellow Umbrella", "Made in China. date 2018-12-01", "€2.15", "€2", true),
                new Parcel(4L, "Lamp", "Made in China. date 2019-02-01", "€20.15", "€2", true),
                new Parcel(5L, "Goblin Statue", "Made in China. date 2012-04-01", "€15", "€2", true),
                new Parcel(6L, "Lamp Vivo", "Made in China. date 2015-02-01", "€20.15", "€2", true),
                new Parcel(7L, "Green Worm", "Made in China. date 2019-09-02", "€20.15", "€2", true),
                new Parcel(8L, "Fake Onion", "Made in China. date 2017-05-01", "€2.15", "€2", true),
                new Parcel(9L, "Glasses", "Made in China. date 2015-01-01", "€15", "€2", false),
                new Parcel(10L, "Hammer", "Made in China. date 2016-06-01", "€15.05", "€2", true),
                new Parcel(11L, "Book", "Made in China. date 2011-08-01", "€15.05", "€2", true),
                new Parcel(12L, "Yellow Vase", "Made in China. date 2009-05-01", "€15.05", "€2", true),
                new Parcel(13L, "Orange Fake Pumkin", "Made in China. date 2007-02-01", "€2.15", "€2", true),
                new Parcel(14L, "Table", "Made in China. date 2014-02-01", "€12.03", "€2", true),
                new Parcel(15L, "Black Table", "Made in China. date 2017-12-02", "€12.03", "€2", false),
                new Parcel(16L, "Green Table", "Made in China. date 2016-01-01", "€20.15", "€2", true),
                new Parcel(17L, "Bed Sheets", "Made in China. date 2014-05-01", "€20.15", "€2", true),
                new Parcel(18L, "Wedding Ring", "Made in China. date 2018-08-01", "€2.15", "€2", true),
                new Parcel(19L, "Trousers", "Made in China. date 2019-01-29", "€12.03", "€2", true),
                new Parcel(20L, "Slim Trousers", "Made in China. date 2017-08-30", "€12.03", "€2", true),
                new Parcel(21L, "Wooden Chair", "Made in China. date 2019-02-01", "€17.03", "€2", false),
                new Parcel(22L, "Metalic Chair", "Made in China. date 2019-04-5", "€17.03", "€2", true),
                new Parcel(23L, "Purple Carpet", "Made in China. date 2018-04-01", "€2.15", "€2", true),
                new Parcel(24L, "Spatula", "Made in China. date 2019-09-01", "€20.15", "€2", true),
                new Parcel(25L, "Microvave", "Made in China. date 2012-03-01", "€20.15", "€2", true),
                new Parcel(26L, "Fake Apple", "Made in China. date 2013-02-01", "€20.15", "€2", true),
                new Parcel(27L, "Fake Moustache", "Made in China. date 2019-04-5", "€20.15", "€2", false),
                new Parcel(28L, "Piano", "Made in China. date 2018-02-01", "€2.15", "€2", false),
                new Parcel(29L, "Violin", "Made in China. date 2019-02-01", "€1.14", "€2", true),
                new Parcel(30L, "PC", "Made in China. date 2017-03-01", "€1.14", "€2", true),
                new Parcel(31L, "Android Phone", "Made in China. date 2019-02-01", "€18.45", "€2", true),
                new Parcel(32L, "Antivirus Software", "Made in China. date 2019-04-5", "€18.45", "€2", true),
                new Parcel(33L, "Pot", "Made in China. date 2018-02-01", "€2.15", "€2", true),
                new Parcel(34L, "Can Opener", "Made in China. date 2019-02-01", "€18.45", "€2", true),
                new Parcel(35L, "Dish Washer", "Made in China. date 2017-03-01", "€18.45", "€2", true),
                new Parcel(36L, "Fairy Cleaner", "Made in China. date 2019-02-01", "€19", "€2", false),
                new Parcel(37L, "Gardening Tool Set", "Made in China. date 2019-05-01", "€19", "€2", true),
                new Parcel(38L, "Gas Stove", "Made in China. date 2018-02-01", "€2.15", "€2", true),
                new Parcel(39L, "Shoes", "Made in China. date 2019-02-01", "€19", "€2", true),
                new Parcel(40L, "Fake Shoes", "Made in China. date 2017-03-01", "€19", "€2", false),
                new Parcel(41L, "Pillow Set", "Made in China. date 2019-02-01", "€78", "€2", false),
                new Parcel(42L, "Diesel Engine", "Made in China. date 2019-04-5", "€78", "€2", false),
                new Parcel(43L, "Fake Cologne", "Made in China. date 2018-02-01", "€2.15", "€2", false),
                new Parcel(44L, "Sausage", "Made in China. date 2019-02-01", "€78", "€2", true),
                new Parcel(45L, "Big Sausage", "Made in China. date 2017-03-01", "€78.18", "€2", true),
                new Parcel(46L, "Popcorn", "Made in China. date 2019-02-01", "€78.18", "€2", true),
                new Parcel(47L, "Flour", "Made in China. date 2019-04-5", "€78.18", "€2", true),
                new Parcel(48L, "Flower", "Made in China. date 2018-02-01", "€2.15", "€2", true),
                new Parcel(49L, "Jacket", "Made in China. date 2019-02-01", "€78.18", "€2", true),
                new Parcel(50L, "Wardrobe", "Made in China. date 2017-03-01", "€78.18", "€2", true)
        );
        return parcels;
    }

    @Override
    public Iterator<? extends Parcel> iterator(long first, long count) {
        List<Parcel> contactsFound = getData();

        return filterContacts(contactsFound).
                subList((int) first, (int) (first + count)).
                iterator();
    }

    private List<Parcel> filterContacts(List<Parcel> contactsFound) {
        return contactsFound;
    }

    @Override
    public long size() {
        return getData().size();
    }

    @Override
    public IModel<Parcel> model(Parcel parcel) {
        return LambdaModel.of(() -> parcel);
    }

}
