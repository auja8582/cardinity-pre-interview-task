package com.cardinity.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.UUID;

/**
 * <p>Void class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Void {

    private UUID id;
    private Date created;
    private Boolean live;
    private UUID parentId;
    private Status status;
    private String error;
    private String orderId;
    private String description;

    /**
     * <p>Constructor for Void.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public Void(String description) {
        this.description = description;
    }

    /**
     * <p>Constructor for Void.</p>
     */
    public Void() {
    }

    public enum Status {

        @SerializedName("approved")
        APPROVED("approved"),
        @SerializedName("declined")
        DECLINED("declined");

        private final String value;

        Status(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>created</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getCreated() {
        return created;
    }

    /**
     * <p>Getter for the field <code>live</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getLive() {
        return live;
    }

    /**
     * <p>Getter for the field <code>parentId</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getParentId() {
        return parentId;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link com.cardinity.model.Void.Status} object.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * <p>Getter for the field <code>error</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getError() {
        return error;
    }

    /**
     * <p>Getter for the field <code>orderId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }
}
