package com.cardinity.oauth;

import com.cardinity.rest.RestResource;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Map;

/**
 * <p>OAuthProvider interface.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public interface OAuthProvider {

    /**
     * <p>buildAuthorizationHeader.</p>
     *
     * @param requestMethod a {@link com.cardinity.rest.RestResource.RequestMethod} object.
     * @param requestUrl a {@link java.lang.String} object.
     * @return a {@link java.lang.String} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    String buildAuthorizationHeader(RestResource.RequestMethod requestMethod, String requestUrl) throws
            GeneralSecurityException, UnsupportedEncodingException;

    /**
     * <p>buildAuthorizationHeader.</p>
     *
     * @param requestMethod a {@link com.cardinity.rest.RestResource.RequestMethod} object.
     * @param requestUrl a {@link java.lang.String} object.
     * @param params a {@link java.util.Map} object.
     * @return a {@link java.lang.String} object.
     * @throws java.security.GeneralSecurityException if any.
     * @throws java.io.UnsupportedEncodingException if any.
     */
    String buildAuthorizationHeader(RestResource.RequestMethod requestMethod, String requestUrl, Map<String, String>
            params) throws
            GeneralSecurityException, UnsupportedEncodingException;

}
