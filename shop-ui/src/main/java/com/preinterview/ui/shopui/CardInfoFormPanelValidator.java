package com.preinterview.ui.shopui;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.FormComponent;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.IFormValidator;

public class CardInfoFormPanelValidator implements IFormValidator {


    @Override
    public FormComponent<?>[] getDependentFormComponents() {
        return new FormComponent[0];
    }

    @Override
    public void validate(Form<?> form) {
        setErrorOnEmptyOrNull(form, "name",
                "please set your name");

        setErrorOnEmptyOrNull(form, "lastName",
                "please set your last name");

        setErrorOnEmptyOrNull(form, "pan",
                "please set pan number");

        setErrorOnEmptyOrNull(form, "cardExpirationDate",
                "please set card's expiration date");

        setErrorOnEmptyOrNull(form, "cvv",
                "please set card's cvv");

    }

    private void setErrorOnEmptyOrNull(Form form, String fieldName,
                                       String errorMsg){
        TextField<String> field = ((TextField<String>)form.get(fieldName));
        if(field.getValue() == null || field.getValue().isEmpty()) {
            form.error(errorMsg);
        }
    }

}
