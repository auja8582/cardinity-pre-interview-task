package com.cardinity.model;

/**
 * <p>Response class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Response {

    private final Integer code;
    private final String body;

    /**
     * <p>Constructor for Response.</p>
     *
     * @param code a {@link java.lang.Integer} object.
     * @param body a {@link java.lang.String} object.
     */
    public Response(Integer code, String body) {
        this.code = code;
        this.body = body;
    }

    /**
     * <p>Getter for the field <code>code</code>.</p>
     *
     * @return a {@link java.lang.Integer} object.
     */
    public Integer getCode() {
        return code;
    }

    /**
     * <p>Getter for the field <code>body</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getBody() {
        return body;
    }
}
