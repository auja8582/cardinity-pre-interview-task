package com.preinterview.ui.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class Customer implements Serializable {

    private String name;

    private String lastName;

    private String cardExpirationDate;

    private String cvv;

    private BigDecimal amount;

    private String pan;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getCardExpirationDate() {
        return cardExpirationDate;
    }

    public void setCardExpirationDate(String cardExpirationDate) {
        this.cardExpirationDate = cardExpirationDate;
    }

    public String getCvv() {
        return cvv;
    }

    public void setCvv(String cvv) {
        this.cvv = cvv;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customer customer = (Customer) o;
        return getName().equals(customer.getName()) &&
                getLastName().equals(customer.getLastName()) &&
                getCardExpirationDate().equals(customer.getCardExpirationDate())
                && getCvv().equals(customer.getCvv());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getLastName(), getCardExpirationDate(),
                getCvv());
    }
}
