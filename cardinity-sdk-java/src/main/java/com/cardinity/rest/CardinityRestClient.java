package com.cardinity.rest;

import com.cardinity.Cardinity;
import com.cardinity.exceptions.CardinityClientException;
import com.cardinity.model.CardinityError;
import com.cardinity.model.Response;
import com.cardinity.model.Result;
import com.cardinity.oauth.OAuthProvider;
import com.cardinity.rest.RestResource.RequestMethod;
import com.google.gson.reflect.TypeToken;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.EntityBuilder;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.conn.HttpClientConnectionManager;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.security.GeneralSecurityException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * <p>CardinityRestClient class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class CardinityRestClient implements RestClient {

    private final OAuthProvider oAuthProvider;

    private HttpClientConnectionManager httpClientConnectionManager;

    private RequestConfig config;

    private HttpClient client;

    /**
     * <p>Constructor for CardinityRestClient.</p>
     *
     * @param oAuthProvider a {@link com.cardinity.oauth.OAuthProvider} object.
     */
    public CardinityRestClient(OAuthProvider oAuthProvider) {
        this.oAuthProvider = oAuthProvider;
        initDefaults();
    }

    /**
     * <p>Constructor for CardinityRestClient.</p>
     *
     * @param oAuthProvider a {@link com.cardinity.oauth.OAuthProvider} object.
     * @param httpClientConnectionManager a {@link org.apache.http.conn.HttpClientConnectionManager} object.
     * @param requestConfig a {@link org.apache.http.client.config.RequestConfig} object.
     * @param httpClient a {@link org.apache.http.client.HttpClient} object.
     */
    public CardinityRestClient(OAuthProvider oAuthProvider,
                               HttpClientConnectionManager httpClientConnectionManager,
                               RequestConfig requestConfig,
                               HttpClient httpClient){
        this.oAuthProvider = oAuthProvider;
        this.httpClientConnectionManager = httpClientConnectionManager;
        this.config = requestConfig;
        this.client = httpClient;
    }

    private final void initDefaults(){
        this.httpClientConnectionManager = new PoolingHttpClientConnectionManager();
        this.config = RequestConfig.custom()
                .setConnectTimeout(80 * 1000)
                .setConnectionRequestTimeout(80 * 1000)
                .setSocketTimeout(80 * 1000).build();
        this.client = HttpClientBuilder.create()
                .setDefaultRequestConfig(config)
                .setConnectionManager(httpClientConnectionManager)
                .build();
    }

    /** {@inheritDoc} */
    @Override
    public <T> Result<T> sendRequest(RequestMethod method, String url, TypeToken<T> clazz, T requestObject) throws GeneralSecurityException, UnsupportedEncodingException {
        return _sendRequest(method, url, clazz, requestObject, null);
    }

    /** {@inheritDoc} */
    @Override
    public <T> Result<T> sendRequest(RequestMethod method, String url, TypeToken<T> clazz) throws GeneralSecurityException, UnsupportedEncodingException {
        return _sendRequest(method, url, clazz, null, null);
    }

    /** {@inheritDoc} */
    @Override
    public <T> Result<T> sendRequest(RequestMethod method, String url, TypeToken<T> clazz, Map<String, String> params) throws GeneralSecurityException, UnsupportedEncodingException {
        return _sendRequest(method, url, clazz, null, params);
    }

    private <T> Result<T> _sendRequest(RequestMethod method, String url, TypeToken<T> clazz, T requestObject,
                                       Map<String, String> params) throws GeneralSecurityException, UnsupportedEncodingException {

        Response response = getResponse(method, url, requestObject, params);

        int responseCode = response.getCode();
        String responseBody = response.getBody();

        Result<T> result;

        if (responseCode < 200 || (responseCode >= 300 && responseCode != 402))
            result = new Result<T>(RestResource.GSON.fromJson(responseBody, CardinityError.class));
        else {
            T resultObject = RestResource.GSON.fromJson(responseBody, clazz.getType());
            result = new Result<T>(resultObject);
        }

        return result;
    }

    private <T> Response getResponse(RequestMethod method, String url, T requestObject, Map<String, String> params) throws GeneralSecurityException, UnsupportedEncodingException {
        HttpURLConnection conn = null;
        int responseCode = 0;
        String responseBody = null;
        HttpResponse response = null;
        HttpUriRequest request = null;
        try {
             if (method == RequestMethod.GET) {
                 request = RequestBuilder.get()
                         .get(createUrl(url, params))
                         .build();
                 addHeaders(request, RequestMethod.GET, url, params);
             } else if (method == RequestMethod.POST) {
                 request = RequestBuilder.get()
                         .post(createUrl(url, params))
                         .setEntity(EntityBuilder.create()
                                 .setText(buildRequestBody(requestObject))
                                 .build())
                         .build();
                 addHeaders(request, RequestMethod.POST, url, params);
             } else if (method == RequestMethod.PATCH) {
                 request = RequestBuilder.get()
                         .patch(createUrl(url, params))
                         .setEntity(EntityBuilder
                                 .create()
                                 .setText(buildRequestBody(requestObject))
                                 .build())
                         .build();
                 addHeaders(request, RequestMethod.PATCH, url, params);
             } else {
                 throw new CardinityClientException("Unrecognized HTTP request type.");
             }

            response = client.execute(request);
            responseCode = response.getStatusLine().getStatusCode();

            if (responseCode >= 200 && responseCode < 300) {
                responseBody = getResponseBody(response.getEntity().getContent());
            } else {
                responseBody = getResponseBody(response.getEntity().getContent());
            }

            return new Response(responseCode, responseBody);

        } catch (UnsupportedEncodingException e) {
            throw new CardinityClientException("UnsupportedEncodingException: failed to encode data in ." + Cardinity
                    .ENCODING_CHARSET);
        } catch (IOException e) {
            throw new CardinityClientException("IOException: check connectivity to cardinity servers.");
        } catch (GeneralSecurityException e) {
            throw new CardinityClientException("OAuthException: failed to sign request.");
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }
    }

    private void addHeaders (HttpUriRequest request, RequestMethod method, String url, Map<String, String> params ) throws GeneralSecurityException, UnsupportedEncodingException {
        getDefaultHeaders().forEach((k, v) -> request.addHeader(k, v));
        if (method == RequestMethod.GET){
            request.addHeader("Authorization", oAuthProvider
                    .buildAuthorizationHeader(method, url, params) );
        } else {
            request.addHeader("Authorization", oAuthProvider
                    .buildAuthorizationHeader(method, url));

            request.addHeader("Content-Type", String
                    .format("application/json;charset=%s", Cardinity
                    .ENCODING_CHARSET));
        }
    }

    private static String getResponseBody(InputStream is) throws IOException {
        Scanner s = new Scanner(is).useDelimiter("\\A");
        String body = s.hasNext() ? s.next() : "";
        is.close();

        return body;
    }

    private static Map<String, String> getDefaultHeaders() {
        Map<String, String> headers = new HashMap<String, String>();

        headers.put("Accept-Charset", Cardinity.ENCODING_CHARSET);
        headers.put("Accept", "application/json");
        headers.put("User-Agent", String.format("cardinity-java-client-%s", Cardinity.VERSION));
        headers.put("Cardinity-Version", Cardinity.API_VERSION);

        return headers;
    }

    private static <T> String buildRequestBody(T object) {
        if (object != null) {
            return RestResource.GSON.toJson(object);
        }
        return "";
    }

    private static String createUrl(String url, Map<String, String> params) throws UnsupportedEncodingException {

        if (params == null) return url;

        StringBuilder queryStringBuffer = new StringBuilder();
        for (Map.Entry<String, String> entry : params.entrySet()) {
            if (queryStringBuffer.length() > 0) {
                queryStringBuffer.append("&");
            }
            queryStringBuffer.append(URLUtils.buildQueryParam(entry.getKey(), entry.getValue()));
        }
        return URLUtils.formatURL(url, queryStringBuffer.toString());
    }
}
