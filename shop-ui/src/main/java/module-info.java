module com.cardinality.shop {
    requires org.apache.wicket.core;
    requires org.apache.wicket.extensions;
    requires org.apache.wicket.util;
    requires org.danekja.jdk.serializable.functional;
    requires org.apache.wicket.request;
    requires cardinity.sdk.java;
    requires java.sql;
    requires wicketstuff.restannotations;
    requires com.google.gson;
    requires wicketstuff.rest.utils;
    requires org.apache.logging.log4j;
    opens com.preinterview.ui.shopui; // a very bad idea, but we need testing
    opens com.preinterview.ui.model;  // a very bad idea, but we need testing
    opens com.preinterview.ui.payment; // a very bad idea, but we need testing
    //TODO: find a way to test wicket without opening modules!!!
}