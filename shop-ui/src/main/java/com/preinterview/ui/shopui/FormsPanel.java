package com.preinterview.ui.shopui;

import org.apache.wicket.markup.html.panel.Panel;

public class FormsPanel extends Panel {
    public FormsPanel(String id) {
        super(id);
        add(new CheckoutFormPanel("checkoutFormPanel")
                .setOutputMarkupId(true));
        this.setOutputMarkupId(true);
        this.setOutputMarkupPlaceholderTag(true);
    }
}
