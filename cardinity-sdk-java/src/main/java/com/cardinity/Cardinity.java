package com.cardinity;

/**
 * <p>Abstract Cardinity class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public abstract class Cardinity {

    /** Constant <code>API_BASE="https://api.cardinity.com"</code> */
    public static final String API_BASE = "https://api.cardinity.com";
    /** Constant <code>API_VERSION="v1"</code> */
    public static final String API_VERSION = "v1";
    /** Constant <code>VERSION="0.1"</code> */
    public static final String VERSION = "0.1";
    /** Constant <code>ENCODING_CHARSET="UTF-8"</code> */
    public static final String ENCODING_CHARSET = "UTF-8";

}
