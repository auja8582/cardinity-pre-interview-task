package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Parcel;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.form.AjaxFallbackButton;
import org.apache.wicket.feedback.ContainerFeedbackMessageFilter;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;

import java.math.BigDecimal;
import java.util.Optional;

public class CheckoutFormPanel extends Panel {

    private Panel parent = this;
    private Form<Parcel> form;

    public CheckoutFormPanel(String id) {
        super(id);
        initForm();
    }

    private void initForm() {
        FormComponent<String> id = new TextField<>("id");
        TextField<String> name = new TextField<>("name");
        TextField<String> price = new TextField<>("price");
        TextField<String> vat = new TextField<>("vat");
        TextField<String> availability =
                new TextField<>("availableInWareHouse");
        TextArea<String> description = new TextArea<>("description");

        form = new Form<>("form", new
                CompoundPropertyModel<>(new Parcel()));
        FeedbackPanel feedbackPanel =
                new FeedbackPanel("feedback",
                        new ContainerFeedbackMessageFilter(this));

        Panel cardInfoPanel = createCardInfoPanel(parent);


        Button button = new AjaxFallbackButton("buyButton", form) {

            @Override
            protected void onError(Optional<AjaxRequestTarget> target) {
                super.onError(target);

                target.get().add(feedbackPanel);
                feedbackPanel.getFeedbackMessages().clear();
            }

            @Override
            protected void onAfterSubmit(Optional<AjaxRequestTarget> target) {
                parent.setOutputMarkupId(true);
                cardInfoPanel.setOutputMarkupId(true);
                ((Panel)getPage().get("formsPanel"))
                        .addOrReplace(cardInfoPanel);
                target.get().add(cardInfoPanel);
            }

        };

        button.setOutputMarkupId(true);
        button.setMarkupId("form-buyButton");

        id.setOutputMarkupId(true).setMarkupId("form-id");

        name.setOutputMarkupId(true).setMarkupId("form-name");
        price.setOutputMarkupId(true).setMarkupId("form-price");
        vat.setOutputMarkupId(true).setMarkupId("form-vat");
        availability.setOutputMarkupId(true).setMarkupId("form-availability");
        description.setOutputMarkupId(true).setMarkupId("form-description");

        form.add(id.add(new TextFieldValidator()));
        form.add(name);
        form.add(price);
        form.add(vat);
        form.add(availability.add(new OutOfStockValidator()));
        form.add(description);
        form.add(button);
        form.setOutputMarkupId(true);
        feedbackPanel.setOutputMarkupPlaceholderTag(true);

        form.add(feedbackPanel);

        add(form);
    }

    public BigDecimal getAmount() {
        Double amount = Double.valueOf(form.getModelObject()
                .getPrice().replaceFirst("€", ""));
        return new BigDecimal(amount);
    }

    private CardInfoFormPanel createCardInfoPanel(Panel parent) {
        CardInfoFormPanel cardInfoFormPanel =
                new CardInfoFormPanel(parent.getId(),
                        CheckoutFormPanel.this);
        return cardInfoFormPanel;
    }

}