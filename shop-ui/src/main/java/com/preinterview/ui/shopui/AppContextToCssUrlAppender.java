package com.preinterview.ui.shopui;

import org.apache.wicket.WicketRuntimeException;
import org.apache.wicket.css.ICssCompressor;
import org.apache.wicket.request.Url;
import org.apache.wicket.request.cycle.RequestCycle;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.PackageResourceReference;
import org.apache.wicket.resource.CssUrlReplacer;
import org.apache.wicket.resource.IScopeAwareTextResourceProcessor;
import org.apache.wicket.util.image.ImageUtil;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Need to adjust to a general case scenario.
 * But this is a proof of a concept in the right direction.
 * */
public class AppContextToCssUrlAppender implements ICssCompressor,
        IScopeAwareTextResourceProcessor {
    private static final Pattern URL_PATTERN =
            Pattern.compile("url\\([ ]*['|\"]?([^ ]*?)['|\"]?[ ]*\\)");
    public static final String EMBED_BASE64 = "embedBase64";
    private final Set<String> excludes = new LinkedHashSet();

    public AppContextToCssUrlAppender() {
    }

    public AppContextToCssUrlAppender(Collection<String> excludes) {
        this.excludes.addAll(excludes);
    }

    public String process(String input, Class<?> scope, String name) {
        Iterator var4 = this.excludes.iterator();

        while(var4.hasNext()) {
            String excludeName = (String)var4.next();
            if (name.endsWith(excludeName)) {
                return input;
            }
        }

        RequestCycle cycle = RequestCycle.get();
        String contextPath = cycle.getRequest().getContextPath();
        Url cssUrl = Url.parse(name);
        Matcher matcher = URL_PATTERN.matcher(input);

        StringBuffer output;
        Object processedUrl;
        boolean embedded;
        for(output = new StringBuffer(); matcher.find(); matcher.appendReplacement(output, embedded ? "url(" + processedUrl + ")" : "url('" + processedUrl + "')")) {
            Url imageCandidateUrl = Url.parse(matcher.group(1));
            embedded = false;
            if (imageCandidateUrl.isFull()) {
                processedUrl = contextPath + imageCandidateUrl
                        .toString(Url.StringMode.FULL);
            } else if (imageCandidateUrl.isContextAbsolute()) {
                processedUrl = contextPath + imageCandidateUrl.toString();
            } else if (imageCandidateUrl.isDataUrl()) {
                embedded = true;
                processedUrl = imageCandidateUrl.toString();
            } else {
                Url cssUrlCopy = new Url(cssUrl);
                cssUrlCopy.resolveRelative(imageCandidateUrl);
                PackageResourceReference imageReference;
                if (cssUrlCopy.getQueryString() != null &&
                        cssUrlCopy.getQueryString().contains("embedBase64")) {
                    embedded = true;
                    imageReference = new PackageResourceReference(scope,
                            cssUrlCopy.toString().replace("?embedBase64",
                                    ""));

                    try {
                        processedUrl = ImageUtil
                                .createBase64EncodedImage(imageReference, true);
                    } catch (Exception var14) {
                        throw new WicketRuntimeException("Error while embedding an image into the css: " + imageReference, var14);
                    }
                } else {
                    imageReference = new PackageResourceReference(scope,
                            cssUrlCopy.toString());
                    processedUrl = cycle.urlFor(imageReference,
                            (PageParameters)null);
                }
            }
        }

        matcher.appendTail(output);
        return output.toString();
    }

    public String compress(String original) {
        throw new UnsupportedOperationException(CssUrlReplacer.class.getSimpleName() + ".process() should be used instead!");
    }

    public Collection<String> getExcludes() {
        return this.excludes;
    }

    public void setExcludes(Collection<String> excludes) {
        this.excludes.clear();
        this.excludes.addAll(excludes);
    }
}
