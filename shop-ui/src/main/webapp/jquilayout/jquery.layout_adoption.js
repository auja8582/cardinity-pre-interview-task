$(document).ready(function(shop) {
    shop.mostInnerLayout = $("#container .middle-center").layout({
        center__paneSelector: ".inner-center",
        west__paneSelector: ".inner-west",
        east__paneSelector: ".inner-east",
        north__paneSelector: "#center-header",
        south__paneSelector: ".ui-layout-south",
        south__resizable: false,
        north__resizable: false,
        south__initClosed: true,
        north__initClosed: true,
        north__spacing_closed: 0,
        south__spacing_closed: 0,
        north__spacing_open: 0,
        west__size: 0,
        east__size: 0,
        south__size: 0,
        spacing_open: 8, // ALL panes
        spacing_closed: 8, // ALL panes
        west__spacing_closed: 12,
        east__spacing_closed: 12,
        south__spacing_closed: 0,
        north__spacing_closed: 0,
        onresize_end: function() {
            $.fn.dataTable
                .tables({
                    visible: true,
                    api: true
                })
                .columns
                .adjust();
        }
    });

    shop.outerLayout = $("#container").layout({
        center__paneSelector: ".outer-center",
        west__paneSelector: ".outer-west",
        east__paneSelector: ".outer-east",
        south__paneSelector: ".ui-layout-south",
        north__paneSelector: ".ui-layout-north",
        west__size: 0,
        north__size: 0,
        south_size: 0,
        east__size: "50%",
        spacing_open: 8, // ALL panes
        spacing_closed: 8, // ALL panes
        north__spacing_open: 0,
        south__spacing_open: 0,
        north__spacing_closed: 0,
        south__spacing_closed: 0,
        west__spacing_open: 0,
        west__spacing_closed: 0,
        north__maxSize: 0,
        south__maxSize: 0,
        west__maxSize: 0,
        west__resizable: false,
        south__resizable: false,
        north__resizable: false,
        south__initClosed: true,
        north__initClosed: true,
        center__childOptions: {
            center__paneSelector: ".middle-center",
            west__size: 0,
            east__size: 0,
            spacing_open: 8, // ALL panes
            spacing_closed: 12 // ALL panes
        },
        onresize_end: function() {
            $.fn.dataTable
                .tables({
                    visible: true,
                    api: true
                })
                .columns
                .adjust();
        }
    });

    $('#table thead tr th').each(function() {
        $(this).css({
            position: 'sticky',
            top: '0px',
            'background-color': 'white',
            'z-index': '1'
        })
    });

    $(".dataTableContainer .table-info").css({
        "position": "sticky",
        "bottom": "0px",
        "background-color": "white",
        "width": "100%",
        "overflow": "hidden",
        "border-top": "1px solid #111"
    });

    throttle = function(fn, wait) {
        var time = Date.now();
        //console.log("inside throttle");
        return function() {
            //console.log("inside throttle closure");
            if ((time + wait - Date.now()) < 0) {
                fn();
                time = Date.now();
            }
        }
    };


    $(window).on("resize", throttle(function() {
        $.fn.dataTable
            .tables({
                visible: true,
                api: true
            })
            .columns
            .adjust();
    }, 200));

    return shop;

}(window.shop.layouts = window.shop.layouts || {}));