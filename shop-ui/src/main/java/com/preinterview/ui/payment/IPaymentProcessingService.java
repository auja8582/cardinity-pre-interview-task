package com.preinterview.ui.payment;

import com.cardinity.model.Payment;
import com.cardinity.model.Result;
import com.preinterview.ui.model.Customer;

import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Map;

public interface IPaymentProcessingService {

    public Result<Payment> createPayment(Customer customer)
            throws GeneralSecurityException, UnsupportedEncodingException;

    public Result<Payment> finalizePayment(Payment payment,
                                           String authorizationData,
                                           Map<String, String> context)
            throws GeneralSecurityException, UnsupportedEncodingException;
}
