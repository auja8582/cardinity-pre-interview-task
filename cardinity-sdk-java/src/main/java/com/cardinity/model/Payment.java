package com.cardinity.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * <p>Payment class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Payment {

    private UUID id;
    private BigDecimal amount;
    private String currency;
    private Date created;
    private Type type;
    private Boolean live;
    private String description;
    private String authorizeData;
    private Boolean settle;
    private Status status;
    private String error;
    private String country;
    private String orderId;
    private Method paymentMethod;
    private PaymentInstrument paymentInstrument;
    private AuthorizationInformation authorizationInformation;

    public enum Status {

        @SerializedName("approved")
        APPROVED("approved"),
        @SerializedName("declined")
        DECLINED("declined"),
        @SerializedName("pending")
        PENDING("pending");

        private final String value;

        Status(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    public enum Method {
        @SerializedName("card")
        CARD("card"),
        @SerializedName("recurring")
        RECURRING("recurring");

        private final String value;

        Method(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    public enum Type {

        @SerializedName("purchase")
        PURCHASE("purchase"),
        @SerializedName("authorization")
        AUTHORIZATION("authorization");

        private final String value;

        Type(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>amount</code>.</p>
     *
     * @return a {@link java.math.BigDecimal} object.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * <p>Setter for the field <code>amount</code>.</p>
     *
     * @param amount a {@link java.math.BigDecimal} object.
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount.setScale(2, BigDecimal.ROUND_DOWN);
    }

    /**
     * <p>Getter for the field <code>currency</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * <p>Setter for the field <code>currency</code>.</p>
     *
     * @param currency a {@link java.lang.String} object.
     */
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    /**
     * <p>Getter for the field <code>created</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getCreated() {
        return created;
    }

    /**
     * <p>Getter for the field <code>type</code>.</p>
     *
     * @return a {@link com.cardinity.model.Payment.Type} object.
     */
    public Type getType() {
        return type;
    }

    /**
     * <p>Getter for the field <code>live</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getLive() {
        return live;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Setter for the field <code>authorizeData</code>.</p>
     *
     * @param authorizeData a {@link java.lang.String} object.
     */
    public void setAuthorizeData(String authorizeData) {
        this.authorizeData = authorizeData;
    }

    /**
     * <p>Setter for the field <code>settle</code>.</p>
     *
     * @param settle a {@link java.lang.Boolean} object.
     */
    public void setSettle(Boolean settle) {
        this.settle = settle;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link com.cardinity.model.Payment.Status} object.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * <p>Getter for the field <code>error</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getError() {
        return error;
    }

    /**
     * <p>Getter for the field <code>country</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCountry() {
        return country;
    }

    /**
     * <p>Setter for the field <code>country</code>.</p>
     *
     * @param country a {@link java.lang.String} object.
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * <p>Setter for the field <code>orderId</code>.</p>
     *
     * @param orderId a {@link java.lang.String} object.
     */
    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    /**
     * <p>Getter for the field <code>orderId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * <p>Getter for the field <code>paymentMethod</code>.</p>
     *
     * @return a {@link com.cardinity.model.Payment.Method} object.
     */
    public Method getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * <p>Setter for the field <code>paymentMethod</code>.</p>
     *
     * @param paymentMethod a {@link com.cardinity.model.Payment.Method} object.
     */
    public void setPaymentMethod(Method paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * <p>Getter for the field <code>paymentInstrument</code>.</p>
     *
     * @return a {@link com.cardinity.model.PaymentInstrument} object.
     */
    public PaymentInstrument getPaymentInstrument() {
        return paymentInstrument;
    }

    /**
     * <p>Setter for the field <code>paymentInstrument</code>.</p>
     *
     * @param paymentInstrument a {@link com.cardinity.model.PaymentInstrument} object.
     */
    public void setPaymentInstrument(PaymentInstrument paymentInstrument) {
        this.paymentInstrument = paymentInstrument;
    }

    /**
     * <p>Getter for the field <code>authorizeData</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getAuthorizeData() {
        return authorizeData;
    }

    /**
     * <p>Getter for the field <code>authorizationInformation</code>.</p>
     *
     * @return a {@link com.cardinity.model.AuthorizationInformation} object.
     */
    public AuthorizationInformation getAuthorizationInformation() {
        return authorizationInformation;
    }

}
