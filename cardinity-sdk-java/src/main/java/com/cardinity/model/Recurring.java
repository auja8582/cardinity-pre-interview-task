package com.cardinity.model;

import java.util.UUID;

/**
 * <p>Recurring class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Recurring implements PaymentInstrument {

    private UUID paymentId;

    /**
     * <p>Getter for the field <code>paymentId</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getPaymentId() {
        return paymentId;
    }

    /**
     * <p>Setter for the field <code>paymentId</code>.</p>
     *
     * @param paymentId a {@link java.util.UUID} object.
     */
    public void setPaymentId(UUID paymentId) {
        this.paymentId = paymentId;
    }

    /** {@inheritDoc} */
    @Override
    public Payment.Method getType() {
        return Payment.Method.RECURRING;
    }
}
