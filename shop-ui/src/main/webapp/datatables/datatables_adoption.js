$(document).ready(function(shop) {
    shop.dataTable = $('#table').DataTable({
        fixedHeader: false,
        paging: true,
        responsive: true,
        pageLength: 17,
        autoWidth: false,
        lengthChange: false,
        bFilter: false,
        select: {
            style: 'single'
        },
        columnDefs: [{
                className: "dt-head-left",
                targets: [0, 1, 2, 3, 4, 5]
            },
            {
                className: "dt-body-left",
                targets: [0, 1, 2, 3, 4, 5]
            }
        ],
        dom: 't<".table-info"ip>'
    });
    shop.dataTable.on('select', function(e, dt, type, indexes) {
        if (type === 'row') {
            $("#form-id").val($(shop.dataTable.rows(indexes)
            .data()[0][0]).text());
            $("#form-name").val($(shop.dataTable.rows(indexes)
            .data()[0][1]).text());
            $("#form-price").val($(shop.dataTable.rows(indexes)
            .data()[0][2]).text());
            $("#form-vat").val($(shop.dataTable.rows(indexes)
            .data()[0][3]).text());
            $("#form-availability").val($(shop.dataTable.rows(indexes)
            .data()[0][4]).text());
            $("#form-description").val($(shop.dataTable.rows(indexes)
            .data()[0][5]).text());
        }
    });

    return shop;

}(window.shop = window.shop || {}));