package com.preinterview.ui.shopui;

import org.apache.wicket.request.resource.CssPackageResource;
import org.apache.wicket.request.resource.CssResourceReference;

import java.util.Locale;

public class ReplaceUrlPackageResourceReference extends CssResourceReference {
    public ReplaceUrlPackageResourceReference(Class<?> scope, String name, Locale locale, String style, String variation) {
        super(scope, name, locale, style, variation);
    }

    public ReplaceUrlPackageResourceReference(Class<?> scope, String name) {
        super(scope, name);
    }

    public CssPackageResource getResource() {
        CssPackageResource resource = new CssPackageResource(this.getScope(), this.getName(), this.getLocale(), this.getStyle(), this.getVariation());
        this.removeCompressFlagIfUnnecessary(resource);
        resource.setCompress(true);
        return resource;
    }
}
