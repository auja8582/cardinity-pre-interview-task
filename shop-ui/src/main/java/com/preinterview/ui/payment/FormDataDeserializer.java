package com.preinterview.ui.payment;

import org.wicketstuff.rest.contenthandling.IObjectSerialDeserial;

public class FormDataDeserializer implements IObjectSerialDeserial<String> {
    @Override
    public String serializeObject(Object o, String s) {
        return o.toString();
    }

    @Override
    public <E> E deserializeObject(String s, Class<E> aClass, String s2) {
        return (E)s;
    }

}
