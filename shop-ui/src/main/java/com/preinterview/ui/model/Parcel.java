package com.preinterview.ui.model;

import java.io.Serializable;
import java.util.Objects;
import java.util.StringJoiner;

public class Parcel implements Serializable {

    private Long id;
    private String name;
    private String description;
    private String price;
    private String vat;
    private Boolean availableInWareHouse;

    public Parcel() {
    }

    public Parcel(Long id, String name, String description, String price,
                  String vat,
                  Boolean availableInWareHouse) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.price = price;
        this.vat = vat;
        this.availableInWareHouse = availableInWareHouse;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public Boolean getAvailableInWareHouse() {
        return availableInWareHouse;
    }

    public void setAvailableInWareHouse(Boolean availableInWareHouse) {
        this.availableInWareHouse = availableInWareHouse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parcel parcel = (Parcel) o;
        return getName().equals(parcel.getName()) &&
                getDescription().equals(parcel.getDescription()) &&
                getPrice().equals(parcel.getPrice()) &&
                getVat().equals(parcel.getVat()) &&
                getAvailableInWareHouse().equals(parcel.getAvailableInWareHouse());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getDescription(), getPrice(), getVat(),
                getAvailableInWareHouse());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ",
                Parcel.class.getSimpleName() + "[", "]")
                .add("id=" + id)
                .add("name='" + name + "'")
                .add("description='" + description + "'")
                .add("price='" + price + "'")
                .add("vat='" + vat + "'")
                .add("availableInWareHouse=" + availableInWareHouse)
                .toString();
    }
}
