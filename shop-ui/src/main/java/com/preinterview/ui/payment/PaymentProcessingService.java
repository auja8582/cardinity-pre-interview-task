package com.preinterview.ui.payment;

import com.cardinity.client.CardinityClient;
import com.cardinity.exceptions.CardinityClientException;
import com.cardinity.exceptions.ValidationException;
import com.cardinity.model.Card;
import com.cardinity.model.Payment;
import com.cardinity.model.Result;
import com.preinterview.ui.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import static com.preinterview.WicketApplication.consumerKey;
import static com.preinterview.WicketApplication.consumerSecret;
import java.io.UnsupportedEncodingException;
import java.security.GeneralSecurityException;
import java.util.Map;
import java.util.stream.Collectors;

public class PaymentProcessingService implements IPaymentProcessingService {

    private static Logger logger =
            LogManager.getLogger(PaymentProcessingService.class);

    private static CardinityClient client =
            new CardinityClient(consumerKey, consumerSecret);

    private Payment createBaseCCPayment(Customer customer) {
        Payment payment = new Payment();
        payment.setCountry("USD");
        payment.setPaymentMethod(Payment.Method.CARD);
        Card card = new Card();
        card.setPan(customer.getPan());
        /**Cvv value feels like a bug cuz' you can enter as many zeroes in
         * front as you like... Lol*/
        //why this is an integer? WTF?
        card.setCvc(Integer.valueOf(customer.getCvv()));
        if(customer.getCardExpirationDate().split("-").length != 3)
            throw new ValidationException("wrong date format.");
        card.setExpYear(Integer.valueOf(customer.getCardExpirationDate()
                .split("-")[0]));
        card.setExpMonth(Integer.valueOf(customer.getCardExpirationDate()
                .split("-")[1]));
        card.setHolder(customer.getName() + " " + customer.getLastName());
        payment.setPaymentInstrument(card);

        return payment;
    }

    @Override
    public Result<Payment> createPayment(Customer customer)
            throws GeneralSecurityException, UnsupportedEncodingException {
        Payment payment = createBaseCCPayment(customer);
        payment.setAmount(customer.getAmount());
        payment.setCountry("LT");
        payment.setCurrency("EUR");
        logger.info("Initiating payment for a customer with id: "
                + ThreadContext.get("customer_id"));
        Result<Payment> result = client.createPayment(payment);
        logger.info("After client.createPayment customer_id: " +
                ThreadContext.get("customer_id"));
        if (result == null) {
            throw new CardinityClientException("check your internet " +
                    "connection.");

        } else if (result.getCardinityError() != null) {
            throw new ValidationException("There were errors in processing " +
                    "your transaction. Contact headquarters or fix issue(s). " +
                    "\nISSUE(S): [" + result.getCardinityError().getErrorItems()
                    .stream().map((s) -> s.getMessage())
                    .collect(Collectors.joining(", "))+"]");
        }
        return result;
    }

    public Result<Payment> finalizePayment(Payment resultPayment,
                                           String authorizationData,
                                           Map<String, String> context)
            throws GeneralSecurityException, UnsupportedEncodingException {
        ThreadContext.putAll(context);
        if (resultPayment.getError() != null) {
            throw new ValidationException(resultPayment.getError());
        } else {
            logger.info("Finalizing payment. START. Transaction id: " +
                    ThreadContext.get("payment_id") + " customer id: " +
                    ThreadContext.get("customer_id"));

            Result<Payment> res = client.finalizePayment(resultPayment.getId(),
                    authorizationData);
            logger.info("Finalizing payment. END. Transaction id: " +
                    ThreadContext.get("payment_id") + " customer id: " +
                    ThreadContext.get("customer_id"));
            return res;
        }
    }
}
