package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Customer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.feedback.ContainerFeedbackMessageFilter;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.form.validation.IFormValidator;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;


public class CardInfoFormPanel extends Panel {

    private CheckoutFormPanel checkoutFormPanel;
    private static final Logger logger =
            LogManager.getLogger(CardInfoFormPanel.class);

    //albeit the class is deprecated it has a nice css style, compared
    //to ModalDialog...
    private ModalWindow modalWindow = new ModalWindow("modalWindow");

    public CardInfoFormPanel(String id,
                             CheckoutFormPanel checkoutFormPanel) {
        super(id);
        this.checkoutFormPanel = checkoutFormPanel;
        add(modalWindow);
        initCardInfoForm();
    }

    private void initCardInfoForm() {
        TextField<String> name = new TextField<>("name");
        TextField<String> lastName = new TextField<>("lastName");
        TextField<String> pan = new TextField<>("pan");
        TextField<String> cardExpirationDate =
                new TextField<>("cardExpirationDate");
        TextField<String> cvv = new TextField<>("cvv");

        Form<Customer> form = new Form<>("cardInfoForm",
                new CompoundPropertyModel<>(new Customer()));

        IFormValidator formValidator = new CardInfoFormPanelValidator();
        FeedbackPanel feedbackPanel =
                new FeedbackPanel("feedback",
                        new ContainerFeedbackMessageFilter(this));
        form.add(formValidator);
        form.add(name);
        form.add(lastName);
        form.add(pan);
        form.add(cardExpirationDate);
        form.add(cvv);
        form.add(new ExecuteTransactionButton("execute", this,
                feedbackPanel, form, checkoutFormPanel, modalWindow, null));

        feedbackPanel.setOutputMarkupPlaceholderTag(true);
        add(feedbackPanel);

        form.add(new AjaxLink<String>("cancel") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                ((Panel) getPage().get("formsPanel"))
                        .addOrReplace(checkoutFormPanel);
                ajaxRequestTarget.add(checkoutFormPanel);
            }
        }.setBody(Model.of("Cancel")));
        add(form);
    }

    public void updateFeedbackMessages(FeedbackPanel feedbackPanel,
                                        AjaxRequestTarget target) {
        getFeedbackMessages().forEach((s) ->
                feedbackPanel.getFeedbackMessages().add(s));
        target.add(feedbackPanel);
        getFeedbackMessages().clear();
    }
}
