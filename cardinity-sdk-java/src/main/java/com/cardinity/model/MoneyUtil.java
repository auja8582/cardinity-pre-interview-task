package com.cardinity.model;

import java.math.BigDecimal;

/**
 * <p>MoneyUtil class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public final class MoneyUtil {

    private MoneyUtil() {
    }

    /**
     * <p>formatAmount.</p>
     *
     * @param amount a {@link java.math.BigDecimal} object.
     * @return a {@link java.math.BigDecimal} object.
     */
    public static BigDecimal formatAmount(BigDecimal amount) {

        if (amount == null)
            return BigDecimal.ZERO.setScale(2, BigDecimal.ROUND_DOWN);

        return amount.setScale(2, BigDecimal.ROUND_DOWN);
    }
}
