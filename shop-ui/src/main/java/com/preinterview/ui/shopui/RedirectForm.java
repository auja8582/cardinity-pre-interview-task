package com.preinterview.ui.shopui;

import org.apache.wicket.behavior.Behavior;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.HiddenField;
import org.apache.wicket.model.Model;

public class RedirectForm extends WebPage {

    public RedirectForm(String casUrl,
                        String paReq,
                        String callbackUrl,
                        String paymentIdentifier) {

        Form form = new Form("ThreeDForm");
        form.add(new HiddenField<String>("PaReq",
                new Model(paReq)));
        form.add(new HiddenField<String>("TermUrl",
                new Model(callbackUrl)));
        form.add(new HiddenField<String>("MD",
                new Model(paymentIdentifier)));
        form.add(Behavior.onTag((component, componentTag) ->
                componentTag.put("action", casUrl)));
        form.setOutputMarkupId(true);
        form.setMarkupId("ThreeDForm");
        add(form);
    }
}
