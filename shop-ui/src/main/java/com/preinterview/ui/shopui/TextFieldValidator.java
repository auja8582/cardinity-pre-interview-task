package com.preinterview.ui.shopui;

import org.apache.wicket.validation.INullAcceptingValidator;
import org.apache.wicket.validation.IValidatable;
import org.apache.wicket.validation.ValidationError;

public class TextFieldValidator implements INullAcceptingValidator {


    @Override
    public void validate(IValidatable validatable) {

        if (validatable.getValue() == null || validatable.getValue()
                .toString().isEmpty()) {
            error(validatable , "test");
        }

    }

    private void error(IValidatable<String> validatable, String errorKey) {
        ValidationError error = new ValidationError();
        error.addKey(getClass().getSimpleName() + "." + errorKey);
        validatable.error(error);
    }
}
