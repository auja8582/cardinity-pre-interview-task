package com.preinterview.ui.shopui;

import com.cardinity.exceptions.CardinityClientException;
import com.cardinity.exceptions.ValidationException;
import com.cardinity.model.CardinityError;
import com.cardinity.model.Payment;
import com.cardinity.model.Result;
import com.preinterview.ui.model.Customer;
import com.preinterview.ui.payment.IPaymentProcessingService;
import com.preinterview.ui.payment.PaymentProcessingService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.extensions.ajax.markup.html.IndicatingAjaxButton;
import org.apache.wicket.extensions.ajax.markup.html.modal.ModalWindow;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.panel.FeedbackPanel;

import static com.cardinity.model.Payment.Status.*;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.preinterview.WicketApplication.*;

public class ExecuteTransactionButton extends IndicatingAjaxButton {
    /**
     * NOTE: static fields are solely for wicket to forbid serialization.
     * .We also could use @Transient , but that would require extra
     * library. and @Transient gives illegal access WARNING, So... f it.
     * . We Also could use LoadableDetachable models but this is a bit
     * too awkward so f it.
     */
    private final Form<Customer> form;
    private final CheckoutFormPanel checkoutFormPanel;
    private final CardInfoFormPanel cardInfoFormPanel;
    //@Transient
    private static IPaymentProcessingService paymentProcessingService;
    private static Payment resultItem;
    //@Transient
    private static Result<Payment> result;
    private final FeedbackPanel feedbackPanel;
    private static final Logger logger =
            LogManager.getLogger(ExecuteTransactionButton.class);
    //albeit the class is deprecated it has a nice css style, compared
    //to ModalDialog...
    private final ModalWindow modalWindow;

    public ExecuteTransactionButton(String id,
                                    CardInfoFormPanel cardInfoFormPanel,
                                    FeedbackPanel feedbackPanel,
                                    Form<Customer> form,
                                    CheckoutFormPanel checkoutFormPanel,
                                    ModalWindow modalWindow,
                                    IPaymentProcessingService
                                            paymentProcessingService) {
        super(id, form);
        this.form = form;
        this.checkoutFormPanel = checkoutFormPanel;
        this.cardInfoFormPanel = cardInfoFormPanel;
        this.feedbackPanel = feedbackPanel;
        this.modalWindow = modalWindow;
        if (paymentProcessingService != null) {
            this.paymentProcessingService = paymentProcessingService;
        } else {
            this.paymentProcessingService =
                    new PaymentProcessingService();
        }
    }

    @Override
    protected void onSubmit(AjaxRequestTarget target) {
        getFeedbackMessages().clear();
        Customer customer = form.getModelObject();
        if (checkoutFormPanel.getAmount() == null ||
                checkoutFormPanel.getAmount()
                        .compareTo(BigDecimal.ZERO) <= 0) {
            error("please, select a parcel");
        }
        customer.setAmount(checkoutFormPanel.getAmount());
        try {
            String customerId = getSession().getId();
            naiveCustomerDB.add(customerId);
            ThreadContext.put("customer_id", customerId);
            result = paymentProcessingService.createPayment(customer);
            resultItem = result.getItem();
            String paymentId = result.getItem().getId().toString();
            ThreadContext.put("payment_id", paymentId);
            initTransaction(customerId, paymentId, resultItem.getStatus(),
                    ThreadContext.getContext());
            //authorization is required...
            if (getTransactionStatus(paymentId) == PENDING) {
                doOpenModalWindow(target, ThreadContext.getContext());
                //no authorization is required...
            } else {
                finalizePayment(result, ThreadContext.getContext());
            }
        } catch (ValidationException e) {
            error("And..." + e.getMessage() + " Please fix issues " +
                    "and try again or call headquarters for " +
                    "assistance.");
            logger.error("faulty transaction. Consumer id: " +
                    ThreadContext.get("customer_id") +
                    ".Transaction id: " +
                    ThreadContext.get("payment_id") + " Reason: " +
                    e.getMessage() + " Error: " + e);
        } catch (UnsupportedEncodingException |
                GeneralSecurityException | CardinityClientException e) {
            error("Ups... The transaction was unsuccessful. Please, " +
                    "contact" + " headquarters for assistance or " +
                    "go away.");
            logger.error("faulty transaction. Consumer id: " +
                    ThreadContext.get("customer_id") +
                    ".Transaction id: " +
                    ThreadContext.get("payment_id") + " Reason: " +
                    e.getMessage() + " Error: " + e);
        } finally {
            updateFeedbackMessages(feedbackPanel, target);
            ThreadContext.clearAll();
        }
    }

    @Override
    protected void onError(AjaxRequestTarget target) {
        updateFeedbackMessages(feedbackPanel, target);
    }

    public void initTransaction(String customerId, String transactionId,
                                Payment.Status transactionStatus,
                                Map<String, String> context) {
        ThreadContext.putAll(context);
        if (!naiveCustomerDB.contains(customerId)) {
            naiveCustomerDB.add(customerId);
            logger.info("adding customer to naiveCustomerDB customer " +
                    "id: " + ThreadContext.get("customer_id"));
        }

        List<String> transactions = naiveCustomerToTransactionDB
                .get(customerId);

        if (transactions == null) {
            transactions = new ArrayList<>();
            logger.info("customer has no transactions yet. Customer id: " +
                    ThreadContext.get("customer_id"));
        }

        transactions.add(transactionId);
        naiveCustomerToTransactionDB.put(customerId, transactions);
        logger.info("associating customer to transaction. Customer id: " +
                ThreadContext.get("customer_id") + " and transaction id: "
                + ThreadContext.get("payment_id"));
        naiveTransactionStatusDB.put(transactionId, transactionStatus);
        logger.info("associating transaction to transaction status. " +
                "Customer id: " +
                ThreadContext.get("customer_id") + " and transaction id: "
                + ThreadContext.get("payment_id") + " and transaction status: "
                + transactionStatus);
    }

    public Payment.Status getTransactionStatus(String transactionId) {
        return naiveTransactionStatusDB.get(transactionId);
    }

    public void finalizePayment(Result<Payment> resultToBeFinalized,
                                Map<String, String> context) {
        ThreadContext.putAll(context);
        if (result.getItem().getStatus() == Payment.Status.PENDING) {
            result = doFinalize(result, ThreadContext.getContext());
            logger.info("payment finalization was successful customer id: "
                    + ThreadContext.get("customer_id") + " transaction id: " +
                    ThreadContext.get("payment_id") + " transaction status: " +
                    result.getItem().getStatus());
        } else {
            result = resultToBeFinalized;
            logger.info("payment finalization was successful customer id: "
                    + ThreadContext.get("customer_id") + " transaction id: " +
                    ThreadContext.get("payment_id") + " no authorization was " +
                    "required. Transaction status: " +
                    result.getItem().getStatus());
        }
        if (result.isValid()) {
            switch (result.getItem().getStatus()) {
                case APPROVED:
                    success("Transaction executed successfully ");
                    logger.info("Transaction executed successfully " +
                            "customer id: " + ThreadContext.get("customer_id")
                            + " transaction id: " +
                            ThreadContext.get("payment_id") + " and status " +
                            APPROVED);
                    break;
                case PENDING:
                    error("Transaction is pending. Contact " +
                            "headquarters");
                    logger.info("Transaction is pending " +
                            " transaction id: "
                            + ThreadContext.get("payment_id") + " and status: "
                            + PENDING +
                            " customer id: " +
                            ThreadContext.get("customer_id"));
                    break;
                case DECLINED:
                    error("Transaction was declined. For more " +
                            "information, please, contact " +
                            "headquarters");
                    logger.info("Transaction was declined id: " +
                            ThreadContext.get("payment_id") + " and status " +
                            DECLINED +
                            " customer id: " +
                            ThreadContext.get("customer_id"));
                    break;
                default:
                    error("Unknown transaction status");
                    logger.error("unknown transaction status " +
                            result.getItem().getStatus() + " id: " +
                            ThreadContext.get("payment_id"));
            }
        } else {
            error("Transaction was declined. For more " +
                    "information, please, contact " +
                    "headquarters");
        }
    }

    private Result<Payment> doFinalize(Result<Payment> result,
                                       Map<String, String> context) {
        ThreadContext.putAll(context);
        String authorization = naiveTransactionAuthorizationDB
                .get(result.getItem().getId().toString());
        logger.info("authorization data retrieved from naiveTransaction" +
                "AuthorizationDB is " + authorization + " for a customer id: " +
                context.get("customer_id") + " and transaction id: "
                + context.get("payment_id"));
        try {
            return paymentProcessingService.finalizePayment(result.getItem(),
                    authorization, context);
        } catch (GeneralSecurityException | UnsupportedEncodingException e) {
            logger.error("customer id: " + context.get("customer_id")
                    + " transaction id: " + context.get("payment_id"), e);
            return null;
        }
    }

    private void setPageCreatorOnModalWindow(Map<String, String> context) {
        //ThreadContext.putAll(context);
        modalWindow.setPageCreator(new ModalWindow.PageCreator() {
            @Override
            public Page createPage() {
                String casURL = resultItem.getAuthorizationInformation()
                        .getUrl();
                String authorizationData = resultItem
                        .getAuthorizationInformation().getData();
                String transactionId = resultItem.getId().toString();
                logger.info("authorization data is " + authorizationData +
                        " for customer with id: " + context
                        .get("customer_id") +
                        " and transaction id: " + context
                        .get("payment_id"));
                return new RedirectForm(casURL,
                        authorizationData,
                        callBackUrl,
                        transactionId);
            }
        });
    }

    private void setCloseWindowCallbackOnModalWindow(Map<String, String>
                                                             context) {
        ThreadContext.putAll(context);
        modalWindow.setWindowClosedCallback(
                new ModalWindow.WindowClosedCallback() {
                    @Override
                    public void onClose(AjaxRequestTarget ajaxRequestTarget) {
                        try {
                            finalizePayment(result, context);
                        } catch (ValidationException e) {
                            error("you must authorize the transaction in " +
                                    "order " +
                                    "to proceed with your purchases");
                        }
                        cardInfoFormPanel.updateFeedbackMessages(feedbackPanel,
                                ajaxRequestTarget);
                    }
                });
    }

    private void doOpenModalWindow(AjaxRequestTarget target,
                                   Map<String, String> context) {
        ThreadContext.putAll(context);
        setPageCreatorOnModalWindow(context);
        modalWindow.show(target);
        logger.info("opened modal window for authorizing payment for " +
                "customer with id: " + ThreadContext.get("customer_id") +
                " and transaction id: " + ThreadContext.get("payment_id"));
        setCloseWindowCallbackOnModalWindow(context);
        logger.info("closed modal window for authorizing payment for " +
                "customer with id: " + ThreadContext.get("customer_id") +
                " and transaction id: " + ThreadContext.get("payment_id"));

    }

    public void updateFeedbackMessages(FeedbackPanel feedbackPanel,
                                       AjaxRequestTarget target) {
        getFeedbackMessages().forEach((s) ->
                feedbackPanel.getFeedbackMessages().add(s));
        target.add(feedbackPanel);
        getFeedbackMessages().clear();
    }
}
