package com.preinterview.ui.shopui;

import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.filter.JavaScriptFilteredIntoFooterHeaderResponse;
import org.apache.wicket.markup.html.IHeaderResponseDecorator;

public class JavaScriptToBodyResponseDecorator implements
        IHeaderResponseDecorator {

    private String containerId;

    public JavaScriptToBodyResponseDecorator(String containerId) {
        this.containerId = containerId;
    }

    @Override
    public IHeaderResponse decorate(IHeaderResponse iHeaderResponse) {
        return new JavaScriptFilteredIntoFooterHeaderResponse(iHeaderResponse,
                containerId);

    }
}
