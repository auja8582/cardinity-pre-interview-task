package com.cardinity.exceptions;

/**
 * <p>ValidationException class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class ValidationException extends CardinityException {

    private static final long serialVersionUID = -2152842451308691156L;

    /**
     * <p>Constructor for ValidationException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public ValidationException(String message) {
        super(message);
    }
}
