package com.cardinity.exceptions;

/**
 * <p>Abstract CardinityException class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public abstract class CardinityException extends RuntimeException {

    private static final long serialVersionUID = -4551051157520060255L;

    /**
     * <p>Constructor for CardinityException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public CardinityException(String message) {
        super(message, null);
    }

}
