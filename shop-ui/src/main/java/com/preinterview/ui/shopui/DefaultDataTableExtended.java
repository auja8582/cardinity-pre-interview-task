package com.preinterview.ui.shopui;

import com.preinterview.ui.model.Parcel;
import org.apache.wicket.extensions.markup.html.repeater.data.table.DataTable;
import org.apache.wicket.extensions.markup.html.repeater.data.table.HeadersToolbar;
import org.apache.wicket.extensions.markup.html.repeater.data.table.IColumn;
import org.apache.wicket.extensions.markup.html.repeater.data.table.ISortableDataProvider;


import java.util.List;

public class DefaultDataTableExtended extends DataTable<Parcel, String> {

    /**
     * Constructor
     *
     * @param id           component id
     * @param iColumns     list of columns
     * @param dataProvider data provider
     * @param rowsPerPage
     */
    public DefaultDataTableExtended(String id,
                                    List<? extends IColumn<Parcel, String>>
                                            iColumns,
                                    ISortableDataProvider<Parcel, String>
                                            dataProvider, int rowsPerPage) {
        super(id, iColumns, dataProvider, rowsPerPage);
        addTopToolbar(new HeadersToolbar<>(this, dataProvider));
    }

}
