package com.preinterview.ui.shopui;

import com.preinterview.WicketApplication;
import org.apache.wicket.markup.head.CssHeaderItem;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.JavaScriptHeaderItem;
import org.apache.wicket.markup.head.filter.HeaderResponseContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public class TripleViewPage extends WebPage {
    public TripleViewPage(final PageParameters parameters) {
        super(parameters);
        add(new HeaderResponseContainer("footer-container",
                "footer-container"));
        add(new DataTablePanel("table"));
        add(new FormsPanel("formsPanel"));
        this.setOutputMarkupId(true);
        //let us assume that session == new Customer
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        response.render(JavaScriptHeaderItem.forReference(
                getApplication().getJavaScriptLibrarySettings()
                        .getJQueryReference()));

        response.render(JavaScriptHeaderItem.forReference(
                getApplication().getJavaScriptLibrarySettings()
                        .getWicketAjaxReference()));

        response.render(JavaScriptHeaderItem.forReference(
                new JavaScriptResourceReference(WicketApplication.class,
                        "/jquilayout/jquery-ui.min.js")));

        response.render(JavaScriptHeaderItem.forReference(
                new JavaScriptResourceReference(WicketApplication.class,
                        "/jquilayout/jquery.layout_and_plugins.min.js")));

        response.render(JavaScriptHeaderItem.forReference(
                new JavaScriptResourceReference(WicketApplication.class,
                        "/jquilayout/jquery.layout_adoption.min.js")));

        response.render(JavaScriptHeaderItem.forReference(
                new JavaScriptResourceReference(WicketApplication.class,
                        "/datatables/datatables.min.js")));

        response.render(JavaScriptHeaderItem.forReference(
                new JavaScriptResourceReference(WicketApplication.class,
                        "/datatables/datatables_adoption.min.js")));

        response.render(CssHeaderItem
                .forReference(new CssResourceReference(WicketApplication.class,
                        "/datatables/datatables.min.css")));

        response.render(CssHeaderItem.forReference(
                new CssResourceReference(WicketApplication.class,
                        "/jquilayout/layout-default.min.css")));

        response.render(CssHeaderItem.forReference(
                new CssResourceReference(WicketApplication.class,
                        "/style/default-form.min.css")));
    }
}
