package com.cardinity.model;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.Date;
import java.util.UUID;

/**
 * <p>Chargeback class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class Chargeback {

    private UUID id;
    private BigDecimal amount;
    private String currency;
    private Date created;
    private Boolean live;
    private UUID parentId;
    private Status status;
    private String error;
    private String orderId;
    private String description;
    private String paymentMethod;
    private String reasonCode;
    private String reasonMessage;
    private String caseId;
    private String arn;

    public enum Status {
        @SerializedName("approved")
        APPROVED("approved"),
        @SerializedName("declined")
        DECLINED("declined"),
        @SerializedName("initiated")
        INITIATED("initiated");

        private final String value;

        Status(final String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }
    }

    /**
     * <p>Getter for the field <code>id</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getId() {
        return id;
    }

    /**
     * <p>Getter for the field <code>amount</code>.</p>
     *
     * @return a {@link java.math.BigDecimal} object.
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * <p>Getter for the field <code>currency</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCurrency() {
        return currency;
    }

    /**
     * <p>Getter for the field <code>created</code>.</p>
     *
     * @return a {@link java.util.Date} object.
     */
    public Date getCreated() {
        return created;
    }

    /**
     * <p>Getter for the field <code>live</code>.</p>
     *
     * @return a {@link java.lang.Boolean} object.
     */
    public Boolean getLive() {
        return live;
    }

    /**
     * <p>Getter for the field <code>parentId</code>.</p>
     *
     * @return a {@link java.util.UUID} object.
     */
    public UUID getParentId() {
        return parentId;
    }

    /**
     * <p>Getter for the field <code>status</code>.</p>
     *
     * @return a {@link com.cardinity.model.Chargeback.Status} object.
     */
    public Status getStatus() {
        return status;
    }

    /**
     * <p>Getter for the field <code>error</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getError() {
        return error;
    }

    /**
     * <p>Getter for the field <code>orderId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getOrderId() {
        return orderId;
    }

    /**
     * <p>Getter for the field <code>description</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getDescription() {
        return description;
    }

    /**
     * <p>Setter for the field <code>description</code>.</p>
     *
     * @param description a {@link java.lang.String} object.
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * <p>Getter for the field <code>paymentMethod</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getPaymentMethod() {
        return paymentMethod;
    }

    /**
     * <p>Setter for the field <code>paymentMethod</code>.</p>
     *
     * @param paymentMethod a {@link java.lang.String} object.
     */
    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    /**
     * <p>Getter for the field <code>reasonCode</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReasonCode() {
        return reasonCode;
    }

    /**
     * <p>Setter for the field <code>reasonCode</code>.</p>
     *
     * @param reasonCode a {@link java.lang.String} object.
     */
    public void setReasonCode(String reasonCode) {
        this.reasonCode = reasonCode;
    }

    /**
     * <p>Getter for the field <code>reasonMessage</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getReasonMessage() {
        return reasonMessage;
    }

    /**
     * <p>Setter for the field <code>reasonMessage</code>.</p>
     *
     * @param reasonMessage a {@link java.lang.String} object.
     */
    public void setReasonMessage(String reasonMessage) {
        this.reasonMessage = reasonMessage;
    }

    /**
     * <p>Getter for the field <code>caseId</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getCaseId() {
        return caseId;
    }

    /**
     * <p>Setter for the field <code>caseId</code>.</p>
     *
     * @param caseId a {@link java.lang.String} object.
     */
    public void setCaseId(String caseId) {
        this.caseId = caseId;
    }

    /**
     * <p>Getter for the field <code>arn</code>.</p>
     *
     * @return a {@link java.lang.String} object.
     */
    public String getArn() {
        return arn;
    }

    /**
     * <p>Setter for the field <code>arn</code>.</p>
     *
     * @param arn a {@link java.lang.String} object.
     */
    public void setArn(String arn) {
        this.arn = arn;
    }
}
