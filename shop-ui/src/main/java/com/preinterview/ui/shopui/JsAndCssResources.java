package com.preinterview.ui.shopui;

import com.preinterview.WicketApplication;
import org.apache.wicket.request.resource.CssResourceReference;
import org.apache.wicket.request.resource.JavaScriptResourceReference;

public class JsAndCssResources {


    /***********DATA TABLES JQUERY PLUGIN START***********************/

    public static final JavaScriptResourceReference dataTablesMinJsReference =
            new JavaScriptResourceReference(WicketApplication.class,
                    "/datatables/datatables.min.js");
    // use with header item (without first /)"datatables/datatables.js"

    public static final JavaScriptResourceReference dataTablesAdoptionJsReference =
            new JavaScriptResourceReference(WicketApplication.class,
                    "/datatables/datatables_adoption.min.js");

    public static final CssResourceReference dataTablesCssMinReference =
            new ReplaceUrlPackageResourceReference(WicketApplication.class,
                    "/datatables/datatables.min.css");

    /***********DATA TABLES JQUERY PLUGIN END*************************/

    /***********JQUERY UI LAYOUT PLUGIN START************************/
    public static final JavaScriptResourceReference jqueryUiMinJsReference =
            new JavaScriptResourceReference(WicketApplication.class,
                    "/jquilayout/jquery.layout_and_plugins.min.js");

    public static final JavaScriptResourceReference jqueryUiUiMinJsReference =
            new JavaScriptResourceReference(WicketApplication.class,
                    "/jquilayout/jquery-ui.min.js");

    public static final JavaScriptResourceReference jqueryUiLayoutAdoption =
            new JavaScriptResourceReference(WicketApplication.class,
                    "/jquilayout/jquery.layout_adoption.min.js");

    public static final CssResourceReference jqueryUiLayoutDefaultCss =
            new CssResourceReference(WicketApplication.class,
                    "/jquilayout/layout-default.min.css");

    public static final CssResourceReference formDefaultCss =
            new CssResourceReference(WicketApplication.class,
                    "/style/default-form.min.css");

    /***********JQUERY UI LAYOUT PLUGIN END**************************/


}
