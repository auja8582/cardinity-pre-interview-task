package com.cardinity;

import com.cardinity.client.CardinityClient;
import com.cardinity.exceptions.ValidationException;
import com.cardinity.model.Card;
import com.cardinity.model.Payment;
import com.cardinity.model.Refund;
import com.cardinity.model.Void;
import com.cardinity.model.Settlement;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.util.UUID;

public class CardinityClientTest {

    static CardinityClient client;

    @BeforeClass
    public static void setUpClass() {
        client = new CardinityClient("consumerKey", "consumerSecret");
    }

    private static Payment createBaseCCPayment() {
        Payment payment = new Payment();
        payment.setCountry("LT");
        payment.setSettle(true);
        payment.setPaymentMethod(Payment.Method.CARD);
        Card card = new Card();
        card.setPan("4111111111111111");
        card.setCvc(123);
        card.setExpYear(2016);
        card.setExpMonth(1);
        card.setHolder("Cardinity Cardinity");
        payment.setPaymentInstrument(card);

        return payment;
    }

    /**
     * Payment validation tests
     */
    @Test(expected = ValidationException.class)
    public void testCreatePaymentValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createPayment(null);
    }

    @Test(expected = ValidationException.class)
    public void testCreatePaymentValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createPayment(new Payment());
    }

    @Test(expected = ValidationException.class)
    public void testFinalizePaymentValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.finalizePayment(null, "authorize_data_test");
    }

    @Test(expected = ValidationException.class)
    public void testFinalizePaymentValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.finalizePayment(UUID.randomUUID(), " ");
    }

    @Test(expected = ValidationException.class)
    public void testFinalizePaymentValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        client.finalizePayment(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testGetPaymentValidationException() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getPayment(null);
    }

    @Test(expected = ValidationException.class)
    public void testGetPaymentsValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getPayments(101);
    }

    @Test(expected = ValidationException.class)
    public void testGetPaymentsValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getPayments(-1);
    }

    /**
     * Settlement validation tests
     */
    @Test(expected = ValidationException.class)
    public void testCreateSettlementValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createSettlement(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateSettlementValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createSettlement(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateSettlementValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        Settlement settlement = new Settlement(BigDecimal.ZERO);
        client.createSettlement(UUID.randomUUID(), settlement);
    }

    @Test(expected = ValidationException.class)
    public void testCreateSettlementValidationException4() throws GeneralSecurityException, UnsupportedEncodingException {
        Settlement settlement = new Settlement(BigDecimal.ONE, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. " +
                "Nulla convallis sem ac nisl " +
                "facilisis mattis non nec nibh. Praesent sit amet dolor libero. Fusce lacus orci, sollicitudin a " +
                "aliquam eu, porta quis velit. Aliquam et ex nisi. Phasellus dictum nisi at est faucibus, eu gravida " +
                "metus mollis.");
        client.createSettlement(UUID.randomUUID(), settlement);
    }

    @Test(expected = ValidationException.class)
    public void testCreateSettlementValidationException5() throws GeneralSecurityException, UnsupportedEncodingException {
        Settlement settlement = new Settlement();
        client.createSettlement(UUID.randomUUID(), settlement);
    }

    @Test(expected = ValidationException.class)
    public void testGetSettlementValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getSettlement(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testGetSettlementValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getSettlement(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testGetSettlementValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getSettlement(null, UUID.randomUUID());
    }

    @Test(expected = ValidationException.class)
    public void testGetSettlementsValidationException() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getSettlements(null);
    }

    /**
     * Void validation tests
     */
    @Test(expected = ValidationException.class)
    public void testCreateVoidValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createVoid(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateVoidValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createVoid(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateVoidValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        Void voidP = new Void("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla " +
                "convallis sem ac nisl " +
                "facilisis mattis non nec nibh. Praesent sit amet dolor libero. Fusce lacus orci, sollicitudin a " +
                "aliquam eu, porta quis velit. Aliquam et ex nisi. Phasellus dictum nisi at est faucibus, eu gravida " +
                "metus mollis.");
        client.createVoid(UUID.randomUUID(), voidP);
    }

    @Test(expected = ValidationException.class)
    public void testGetVoidValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getVoid(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testGetVoidValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getVoid(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testGetVoidValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getVoid(null, UUID.randomUUID());
    }

    @Test(expected = ValidationException.class)
    public void testGetVoidsValidationException() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getVoids(null);
    }

    /**
     * Refund validation tests
     */
    @Test(expected = ValidationException.class)
    public void testCreateRefundValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createRefund(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateRefundValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.createRefund(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateRefundValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        Refund refund = new Refund(BigDecimal.ZERO);
        client.createRefund(UUID.randomUUID(), refund);
    }

    @Test(expected = ValidationException.class)
    public void testCreateRefundValidationException4() throws GeneralSecurityException, UnsupportedEncodingException {
        Refund refund = new Refund(BigDecimal.ONE, "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla convallis sem ac nisl " +
                "facilisis mattis non nec nibh. Praesent sit amet dolor libero. Fusce lacus orci, sollicitudin a " +
                "aliquam eu, porta quis velit. Aliquam et ex nisi. Phasellus dictum nisi at est faucibus, eu gravida " +
                "metus mollis.");
        client.createRefund(UUID.randomUUID(), refund);
    }

    @Test(expected = ValidationException.class)
    public void testCreateRefundValidationException5() throws GeneralSecurityException, UnsupportedEncodingException {
        Refund refund = new Refund();
        client.createRefund(UUID.randomUUID(), refund);
    }

    @Test(expected = ValidationException.class)
    public void testGetRefundValidationException1() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getRefund(null, null);
    }

    @Test(expected = ValidationException.class)
    public void testGetRefundValidationException2() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getRefund(UUID.randomUUID(), null);
    }

    @Test(expected = ValidationException.class)
    public void testGetRefundValidationException3() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getRefund(null, UUID.randomUUID());
    }

    @Test(expected = ValidationException.class)
    public void testGetRefundsValidationException() throws GeneralSecurityException, UnsupportedEncodingException {
        client.getRefunds(null);
    }

    /**
     * CardinityClient constructor validations test
     */
    @Test(expected = ValidationException.class)
    public void testCreateClientException1() {
        new CardinityClient(null, "test");
    }

    @Test(expected = ValidationException.class)
    public void testCreateClientException2() {
        new CardinityClient("", "test");
    }

    @Test(expected = ValidationException.class)
    public void testCreateClientException3() {
        new CardinityClient("test", null);
    }

    @Test(expected = ValidationException.class)
    public void testCreateClientException4() {
        new CardinityClient("test", "");
    }

    @Test(expected = ValidationException.class)
    public void testCreateClientException5() {
        new CardinityClient("", "");
    }

    @Test(expected = ValidationException.class)
    public void testCreateClientException6() {
        new CardinityClient(null, null);
    }

}
