package com.preinterview.ui.shopui;

import com.preinterview.WicketApplication;
import org.apache.wicket.util.tester.WicketTester;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;

class TripleViewPageTest {
    private WicketTester tester;

    @BeforeEach
    public void setUp() {
        File curDirectory = new File(System.getProperty("user.dir"));
        File webContextDir = new File(curDirectory, "src/main/webapp");
        tester = new WicketTester(new WicketApplication(),
                webContextDir.getAbsolutePath());
        tester.getApplication().getResourceSettings()
                .setThrowExceptionOnMissingResource(false);

    }

    @Test
    public void homepageRendersSuccessfully() {

        //start and render the test page
        tester.startPage(TripleViewPage.class);

        //assert rendered page class
        tester.assertRenderedPage(TripleViewPage.class);
    }
}