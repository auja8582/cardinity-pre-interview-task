package com.cardinity.exceptions;

/**
 * <p>CardinityClientException class.</p>
 *
 * @author antiadministratorius
 * @version $Id: $Id
 */
public class CardinityClientException extends CardinityException {

    private static final long serialVersionUID = 5273072716090618133L;

    /**
     * <p>Constructor for CardinityClientException.</p>
     *
     * @param message a {@link java.lang.String} object.
     */
    public CardinityClientException(String message) {
        super(message);
    }
}
